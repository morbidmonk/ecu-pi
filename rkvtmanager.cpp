#include "rkvtmanager.h"
#include <QDebug>
#include <QDataStream>

#include <QByteArray>
#include <QFile>

rkVtManager::rkVtManager(QObject *parent) : QObject(parent)
{
    poolVersion.resize(7);
    poolVersion = QByteArray("0000009");
    store = true;

    addressManager = nullptr;
    state = 0;

    statusTimer.setSingleShot(true);

    connect(&statusTimer, &QTimer::timeout, this, &rkVtManager::statusTimerTimeout);
    connect(&maintainanceTimer, &QTimer::timeout, this, &rkVtManager::maintainanceTimerTimeout);

    ourVersion = 3;

    WS_Initialized = false;

    sendPoolState = 0;
}

void rkVtManager::init(quint8 vt)
{
    WS_Initialized = false;
    instance = vt;
    address = 254;  // Null till found

    if(addressManager == nullptr) return;

    tp_out = new transportProtocol(&addressManager->address,TP_SEND);
    etp_out = new extendedTransportProtocol(&addressManager->address, true);

    tp_in = new transportProtocol(&addressManager->address,TP_RECEIVE);

    connect(tp_out, qOverload<canFrame>(&transportProtocol::frameToSend), this, &rkVtManager::sendCANframe);
    connect(etp_out, qOverload<canFrame>(&extendedTransportProtocol::frameToSend), this, &rkVtManager::sendCANframe);

    connect(etp_out, qOverload<bool>(&extendedTransportProtocol::sent), this, &rkVtManager::etpSent);

    connect(tp_in, qOverload<canFrame>(&transportProtocol::frameToSend), this, &rkVtManager::sendCANframe);
    connect(tp_in, qOverload<transportProtocol&>(&transportProtocol::received), this, &rkVtManager::tpReceived);

    for(int i=0; i<addressManager->addressTable.size();i++)
    {
        // VT = Group 2, Class 0, Function 29, Func Instance = vt

        rkClaimedAddress ca = addressManager->addressTable[i];

        if(ca.name.industry == 2 && ca.name.devClass == 0 && ca.name.devFunction == 29 && ca.name.devFunctionInstance == vt)
        {
            // Found it
            address = ca.address;
            name = ca.name;
            state = 2;  // Waiting for initial VT status
            statusTimer.start(30000);   // 30 secs
            qDebug() << "VT Found";
            return;
        }
    }

    // Not found
    qDebug() << "VT Not Present";
    state = 1;    // Wait for an address claim from VT
}

// State 0 = Nuttin
// 1 = Waiting for address claim
// 2 = waiting for vt status
// 3 = waiting for getmem response
// 4 = waiting for getsoftkeys response
// 5 = waiting for gethardwareresponse
// 200 = ready to rock

bool rkVtManager::processCANframe(canFrame f)
{
    // returns true if was a frame from this VT

    if(state == 0) return false;

    if(state == 1)
    {
        // Looking for an address claim
        if(f.pgn == 60928) // Address Claimed
        {
            rkClaimedAddress ca;
            ca.address = f.sa;
            QDataStream ds(f.data);
            ds.setByteOrder(QDataStream::LittleEndian);
            quint64 tn;
            ds >> tn;
            ca.name.setNAME(tn);

            if(ca.name.industry == 2 && ca.name.devClass == 0 && ca.name.devFunction == 29 && ca.name.devFunctionInstance == instance)
            {
                // Found it
                address = ca.address;
                name = ca.name;
                state = 2;  // Waiting for initial VT status
                statusTimer.start(30000);   // 30 secs
                qDebug() << "VT Found";
            }
        }
        return false;
    }

    // ?? Check for an change of address for this VT ?? will this ever happen ??

    if(f.sa != address) return false; // Not from this VT

    tp_in->processFrame(f);
    tp_out->processFrame(f);
    etp_out->processMessage(f);

    if(f.pgn == 58880)
    {
        QDataStream ds(f.data);
        ds.setByteOrder(QDataStream::LittleEndian);

        quint8 cmd;

        ds >> cmd;

        if(f.ps == addressManager->address || f.ps == 255)
        {
            processCommand(f.data);

            return true;
        }
    }
    return false;
}

void rkVtManager::etpSent(bool ok)
{
    if(state == 201)
    {
        if(ok)
        {
            // Send end of object pool
            canFrame tx;
            tx.pf = 231;
            tx.ps = address;
            tx.sa = addressManager->address;
            tx.priority = 5;
            tx.dlc = 8;
            tx.data[0] = 18;

            emit sendCANframe(tx);

            sendPoolState = 0;
            state = 202;
        }
        else
        {
            // Try again???
        }
    }
}

void rkVtManager::sendObjectPool(QByteArray &pool)
{
    objectPool = pool;

    // NEW: GetVersions then if version is present, load that, if not transfer pool

    //
    canFrame tx;
    tx.pf = 231;
    tx.ps = address;
    tx.sa = addressManager->address;
    tx.priority = 5;
    tx.dlc = 8;
    tx.data[0] = (quint8)223;
    emit sendCANframe(tx);

    sendPoolState = 1;
}

void rkVtManager::statusTimerTimeout()
{
    // VT has gone!
    qDebug() << "VT Shutdown";
    state = 1;
    WS_Initialized = false; // Is this right? would this affect connection to TC?
    emit vtLost();
}

void rkVtManager::maintainanceTimerTimeout()
{
    // Check state and then send WSM
    if(state >= 3)    // Was 100 changed to 3 so we send them earlier as part of Gen4 Stuck Fix
    {
        canFrame f;
        f.pf = 231;
        f.ps = address; // VT Address
        f.sa = addressManager->address;
        f.priority = 5;
        f.dlc = 8;
        f.data[0] = (quint8)255;    // Working Set Maint Message
        if(WS_Initialized) f.data[1] = 0;
        else f.data[1] = 1;
        f.data[2] = ourVersion;      // Part 6 Version
        emit sendCANframe(f);

        WS_Initialized = true;
    }
}

void rkVtManager::tpReceived(transportProtocol &tp)
{
    int pgn = tp.pgn;
    QByteArray data = tp.data;

   // qDebug() << "TP Received: PGN=" << pgn;

    if(pgn == 58880)
    {
       // qDebug() << "VT -> ECU command=" << data[0];

        processCommand(data);

        //emit cmdReceived(data);

    }
}

void rkVtManager::queue_cmd(QByteArray cmd)
{
    // if queue is empty then send straight away
    if(commandQueue.isEmpty())
    {
        commandQueue.enqueue(cmd);  // Put it in queue so we can compare to response..

        if(cmd.length() <= 8)
        {
            canFrame tx;
            tx.pf = 231;
            tx.ps = address;
            tx.sa = addressManager->address;
            tx.priority = 5;
            tx.dlc = 8;
            tx.data = cmd;
            emit sendCANframe(tx);
        }
        else
        {   // Use TP
            tp_out->send(address, 59136, cmd);
        }
        return;
    }

    commandQueue.enqueue(cmd);
}

void rkVtManager::cmd_ChangeNumericValue(quint16 id, quint32 v)
{
    QByteArray t;
    t.resize(8);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)168 << id << (quint8)0xFF << v;

    queue_cmd(t);
}

void rkVtManager::cmd_ChangeStringValue(quint16 id, QString v)
{
    QByteArray t;
    t.resize(5);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)179 << id << (quint16)v.length();

    t+=v.toLocal8Bit();

    queue_cmd(t);
}

void rkVtManager::processCommand(QByteArray &data)
{
    QDataStream ds(data);
    ds.setByteOrder(QDataStream::LittleEndian);

    quint8 cmd;

    ds >> cmd;

    if(state > 2)
    {
        // Looking for status

        if( cmd == 254) // VT Status
        {
            statusTimer.start(3000); // 3 second timeout as per spec
        }

    }

    if(state == 2)
    {

            if( cmd == 254) // VT Status
            {
                state = 3;
                statusTimer.start(3000); // 3 second timeout as per spec
                qDebug() << "VT is Alive";

                // Now as part of Gen4 Stuck Fix, move Working Set Master and Initial Working Set Maintenance message here
                canFrame rtx;
                rtx.pf = 254;
                rtx.ps = 13;
                rtx.sa = addressManager->address;
                rtx.priority = 7;
                rtx.dlc = 8;
                rtx.data[0] = 1; // Just us in the set

                emit sendCANframe(rtx);
                maintainanceTimerTimeout();
                maintainanceTimer.start(1000);

                // Get memory (0) to get VT Version
                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = (quint8)192;
                tx.data[2] = 0;
                tx.data[3] = 0;
                tx.data[4] = 0;
                tx.data[5] = 0;
                emit sendCANframe(tx);
            }

        return;
    }

    if(state == 3)
    {

            if( cmd == 192) // Get Memory Response
            {
                vtVersion = data[1];

                qDebug() << "VT Version " << vtVersion;

                state = 4;

                // Get # Softkeys
                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = (quint8)194;
                emit sendCANframe(tx);
            }

        return;
    }

    if(state == 4)
    {

            if( cmd == 194)   // Get # Softkeys response
            {

                state = 5;
                // Get Hardware
                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = (quint8)199;
                emit sendCANframe(tx);
            }

        return;
    }

    if(state == 5)
    {

            if( cmd == 199)   // Get Hardware response
            {



                quint8 boottime;
                quint8 mode;
                quint8 hardware;
                quint16 width;
                quint16 height;


                ds >> boottime >> mode >> hardware >> width >> height;

                qDebug() << "VT Hardware: Boot Time=" << boottime << " Mode=" << mode << "Size=" << width <<"x" << height;

                state = 100;
                // Now send WSM?
                // No, moved earlier into state 3 so we don't hang on GetVersions on the Gen 4

                emit vtFound();

                QByteArray pool;
                QFile file("sprayer2.obp");
                if(file.open(QIODevice::ReadOnly))
                {
                    pool = file.readAll();
                    file.close();
                    this->sendObjectPool(pool);
                }
                else
                {
                    qDebug() << "Couldn't open object pool file";
                }
           }

        return;
    }

    if(state == 100)
    {   // Objectpool load/transfer

        if(cmd == 224)
        {
            // Get Versions Response
            quint8 nVersions;
            ds >> nVersions;

            bool hasPool = false;

            for(int i=0;i<nVersions;i++)
            {
                char buff[7];
                ds.readRawData(buff,7);
                QByteArray str(buff,7);
                if(str==poolVersion)
                {
                    hasPool = true;
                    break;
                }
            }

            if(hasPool)
            {
                // LoadVersion
                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = 209;

                for(int i=0;i<7;i++)
                {
                    tx.data[i+1] = poolVersion.at(i);
                }

                emit sendCANframe(tx);
                state = 101;
            }
            else
            {
                // Transfer Pool, start with get memory
                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = (quint8)192;
                tx.data[2] = objectPool.length() & 0xFF;
                tx.data[3] = (objectPool.length() >> 8) & 0xFF;
                tx.data[4] = (objectPool.length() >> 16) & 0xFF;
                tx.data[5] = (objectPool.length() >> 24) & 0xFF;
                state = 200;
                emit sendCANframe(tx);

            }

            return;
        }

    }

    if(state == 101)
    {   // Waiting Load Version Response
        if(cmd == 209)
        {
            if(data.at(5) == 0)
            {
                // Success
                state = 1000;
                emit poolSent();
            }
            else
            {
                qDebug() << "Failed to LoadVersion";
            }
            return;
        }

    }

    if(state == 200)
    {
        if( cmd == 192) // Get Memory Response
        {

                etp_out->send(address, 59136, objectPool);
                sendPoolState = 2;

                state = 201;

                QByteArray t(8,0xff);
                t[0]=18;
                //commandQueue.enqueue(t);    // Push a false send end object pool command so we only send following commands once pool sent
                return;
        }

    }

    if(state == 201)
    {
        return;
    }

    if(state==202)
    {
        if(cmd==18)
        {

            if(store)
            {
                state = 203;

                canFrame tx;
                tx.pf = 231;
                tx.ps = address;
                tx.sa = addressManager->address;
                tx.priority = 5;
                tx.dlc = 8;
                tx.data[0] = 208;
                for(int i=0;i<7;i++)
                {
                    tx.data[i+1] = poolVersion.at(i);
                }

                emit sendCANframe(tx);

            }
            else
            {
                state = 1000;
                emit poolSent();
            }
        }
        return;
    }

    if(state==203)
    {
        if(cmd==208)
        {
            state = 1000;
             emit poolSent();
            return;
        }
    }

    if(state != 1000) return;


        if( !commandQueue.isEmpty() )
        {
            QByteArray t = commandQueue.head();

            if( data[0] == t[0] )
            {
                commandQueue.dequeue();
                if(!commandQueue.isEmpty())
                {
                    QByteArray cmd = commandQueue.head();
                    if(cmd.length() <= 8)
                    {
                        canFrame tx;
                        tx.pf = 231;
                        tx.ps = address;
                        tx.sa = addressManager->address;
                        tx.priority = 5;
                        tx.dlc = 8;
                        tx.data = cmd;
                        emit sendCANframe(tx);
                    }
                    else
                    {   // Use TP
                        tp_out->send(address, 59136, cmd);
                    }
                }
                return;
            }
        }

        emit cmdReceived(data);
    return;
}
