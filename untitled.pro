QT -= gui

CONFIG += c++14 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        canframe.cpp \
        ecu.cpp \
        extendedtransportprotocol.cpp \
        main.cpp \
        mixcalc.cpp \
        rkaddressmanager.cpp \
        rkauxfunctionmanager.cpp \
        rkclaimedaddress.cpp \
        rkname.cpp \
        rknativeeventfilter.cpp \
        rkvtmanager.cpp \
        transportprotocol.cpp \
        usb/usbDevice.cpp \
        usb/utils.cpp \
        vt_commands.cpp

LIBS += -lusb-1.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /home/pi/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    canframe.h \
    ecu.h \
    extendedtransportprotocol.h \
    mixcalc.h \
    rkaddressmanager.h \
    rkauxfunctionmanager.h \
    rkclaimedaddress.h \
    rkname.h \
    rknativeeventfilter.h \
    rkvtmanager.h \
    transportprotocol.h \
    usb/usbDevice.h \
    usb/utils.h \
    vt_commands.h
