#ifndef TRANSPORTPROTOCOL_H
#define TRANSPORTPROTOCOL_H

#include <QObject>
#include <QTimer>
#include <canframe.h>
#include <QByteArray>




enum tpMode {TP_SEND=0, TP_RECEIVE=1};
#define TP_T1 750
#define TP_T2 1250
#define TP_T3 1250
#define TP_T4 1050


class transportProtocol : public QObject
{
 Q_OBJECT
public:
    transportProtocol(quint8 *a, tpMode m = TP_RECEIVE);
    tpMode mode;
    bool active;
    //int ourAddress;

    bool connected; // Connection Active
    int remoteAddress;  // Address of the other CF
    quint8 *ownAddress;    // Own Address

    bool onHold;

    int pgn;
    int totalBytes;
    int totalPackets;
    int maxSend;

    int currentPacket;
    int receivedBytes;
    int lastPacketSize;

    int dpo;
    bool haveDPO;

    int count;  // Number of packets received during current CTS
    int requestedCount; // Number of packets we requested in current CTS
    QByteArray data;
    QTimer timer;   // Timer for either sending DT or Detecting a timeout recving DT
    QTimer ctsTimer;    //

public slots:
    void processFrame(canFrame frame);
    void sendTimerTimeout();
    void ctsTimerTimeout();
    void send(int dest, int s_pgn, const QByteArray &s_data);
private slots:
    void abort(int reason);
signals:
    void frameToSend(canFrame);
    void received(transportProtocol &);
    void sent(bool);
};


#endif // TRANSPORTPROTOCOL_H
