#ifndef CANFRAME_H
#define CANFRAME_H

#include <QObject>
#include <QByteArray>


class canFrame
{
public:
    canFrame();
    void setPGN(int p);

public:
    int priority;
    bool edp;
    bool dp;
    bool ide;
    int pf;
    int ps;
    int sa;
    int dlc;
    QByteArray data;
    int pgn;

};

Q_DECLARE_METATYPE(canFrame);

#endif // CANFRAME_H
