#include "transportprotocol.h"
#include <QDebug>

transportProtocol::transportProtocol(quint8 *a, tpMode m) : QObject(0)
{
    ownAddress = a;
    connected = false;
    remoteAddress = 0;
    mode = m;
    active = false;
    totalBytes = 0;
    totalPackets = 0;
   // sa = 0;
   // da = 0;
    pgn = 0;
    maxSend = 255;
    currentPacket = 0;
    receivedBytes = 0;
    count = 0;
    lastPacketSize = 0;
    data.clear();
    haveDPO = false;
    dpo = 0;
    connect(&timer,SIGNAL(timeout()),this,SLOT(sendTimerTimeout()));
    connect(&ctsTimer,SIGNAL(timeout()),this,SLOT(ctsTimerTimeout()));
    ctsTimer.setSingleShot(true);
}


void transportProtocol::send(int dest, int s_pgn, const QByteArray &s_data)
{
    if(active) return;
    data = s_data;

    connected = false;

    totalBytes = data.count();
    totalPackets = totalBytes / 7;
    lastPacketSize = totalBytes - (totalPackets*7);
    if(lastPacketSize > 0) totalPackets++;

    //qDebug() << "RTS: totalBytes     " << totalBytes;
    //qDebug() << "RTS: totalPackets   " << totalPackets;
    //qDebug() << "RTS: lastPacketSize " << lastPacketSize;

    remoteAddress = dest;
    pgn = s_pgn;

    active = true;
    count = 0;

    canFrame  rtsFrame;



    rtsFrame.sa = *ownAddress;
    rtsFrame.pf = 236;
    rtsFrame.ps = remoteAddress;
    rtsFrame.priority = 7;
    rtsFrame.dlc = 8;
    rtsFrame.data[0] = 16;  // RTS
    rtsFrame.data[1] = totalBytes & 0xFF;
    rtsFrame.data[2] = (totalBytes & 0xFF00) >> 8;
    rtsFrame.data[3] = totalPackets;
    rtsFrame.data[4] = 255;   // Max packets we can send in response to a CTS
    rtsFrame.data[5] = pgn & 0xFF;
    rtsFrame.data[6] = (pgn & 0xFF00 ) >> 8;
    rtsFrame.data[7] = (pgn & 0xFF0000 ) >> 16;

    rtsFrame.pgn = 60416;

    emit frameToSend(rtsFrame);
    ctsTimer.start(1250);
}

void transportProtocol::ctsTimerTimeout()
{


    if(mode == TP_SEND)
    {
        if(!connected)
        {
            // No Response to RTS
            //qDebug() << "TP Timeout: No Response to RTS";
            active = false;
            return;
        }
        if(onHold)
        {   // T4 1050ms - Lack of CTS after CTS(0) "Hold Connection Open"
            //qDebug() << "TP Aborting: CTS Timeout while on Hold";
            // Send Abort Reason 3
            connected = false;
            active = false;

            canFrame abortFrame;
            abortFrame.sa = *ownAddress;
            abortFrame.pf = 236;
            abortFrame.ps = remoteAddress;
            abortFrame.priority = 7;
            abortFrame.dlc = 8;
            abortFrame.data[0] = 255;  // CTS
            abortFrame.data[1] = 3;   // Reason (Reject connection, can't handle another)
            abortFrame.data[2] = 0xFF;   // Resvd
            abortFrame.data[3] = 0xFF;    // Resvd
            abortFrame.data[4] = 0xFF;    // Resvd
            abortFrame.data[5] = pgn & 0xFF;
            abortFrame.data[6] = (pgn & 0xFF00 ) >> 8;
            abortFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
            emit frameToSend(abortFrame);
            //qDebug() << "TP Timeout: CTS Timeout while on Hold";

            return;
        }
        else
        {   // T3 1250ms - Lack of CTS or ACK after we have sent last packet
            //qDebug() << "TP Aborting: No CTS or ACK after last packet";
            // Send Abort Reason 3
            connected = false;
            active = false;

            canFrame abortFrame;
            abortFrame.sa = *ownAddress;
            abortFrame.pf = 236;
            abortFrame.ps = remoteAddress;
            abortFrame.priority = 7;
            abortFrame.dlc = 8;
            abortFrame.data[0] = 255;  // CTS
            abortFrame.data[1] = 3;   // Reason (Reject connection, can't handle another)
            abortFrame.data[2] = 0xFF;   // Resvd
            abortFrame.data[3] = 0xFF;    // Resvd
            abortFrame.data[4] = 0xFF;    // Resvd
            abortFrame.data[5] = pgn & 0xFF;
            abortFrame.data[6] = (pgn & 0xFF00 ) >> 8;
            abortFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
            emit frameToSend(abortFrame);
            //qDebug() << "TP Timeout: No CTS or ACK after last packet";

            return;
        }
    }

    if(!connected) return;
    if( mode == TP_RECEIVE)
    {
        if(receivedBytes == 0)
        {    // T2 1250ms - Lack of first DT after sending CTS
            //qDebug() << "TP Aborting: DT Timeout after CTS";
            // Send ABort Reason 3
            connected = false;

            canFrame abortFrame;
            abortFrame.sa = *ownAddress;
            abortFrame.pf = 236;
            abortFrame.ps = remoteAddress;
            abortFrame.priority = 7;
            abortFrame.dlc = 8;
            abortFrame.data[0] = 255;  // CTS
            abortFrame.data[1] = 3;   // Reason (Reject connection, can't handle another)
            abortFrame.data[2] = 0xFF;   // Resvd
            abortFrame.data[3] = 0xFF;    // Resvd
            abortFrame.data[4] = 0xFF;    // Resvd
            abortFrame.data[5] = pgn & 0xFF;
            abortFrame.data[6] = (pgn & 0xFF00 ) >> 8;
            abortFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
            emit frameToSend(abortFrame);
            //qDebug() << "TP Timeout: No DT after CTS";

            return;
        }
        else
        {    // T1 750ms - Lack of DT after previous when more expected
            //qDebug() << "TP Aborting: Next DT Timeout";
            // Send Abort Reason 3
            connected = false;

            canFrame abortFrame;
            abortFrame.sa = *ownAddress;
            abortFrame.pf = 236;
            abortFrame.ps = remoteAddress;
            abortFrame.priority = 7;
            abortFrame.dlc = 8;
            abortFrame.data[0] = 255;  // CTS
            abortFrame.data[1] = 3;   // Reason (Reject connection, can't handle another)
            abortFrame.data[2] = 0xFF;   // Resvd
            abortFrame.data[3] = 0xFF;    // Resvd
            abortFrame.data[4] = 0xFF;    // Resvd
            abortFrame.data[5] = pgn & 0xFF;
            abortFrame.data[6] = (pgn & 0xFF00 ) >> 8;
            abortFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
            emit frameToSend(abortFrame);
            //qDebug() << "TP Timeout: Next DT";

            return;
        }
    }
}

void transportProtocol::sendTimerTimeout()
{
    if(count == 0)
    {
        timer.stop();
        return;
    }

    // Send next packet;
    canFrame dtFrame;
    int bytesToSend = 7;
    if( (currentPacket == totalPackets) && lastPacketSize > 0)
    {
        for(int i=1;i<8;i++)
        {
            dtFrame.data[i] = 0xFF;
        }
        bytesToSend = lastPacketSize;
    }

    int address = (currentPacket-1)*7;

    for(int i = 0; i < bytesToSend ; i++)
    {
        dtFrame.data[i+1] = data[address++];
    }

    dtFrame.data[0] = currentPacket;

    dtFrame.sa = *ownAddress;

    dtFrame.pf = 235;
    dtFrame.ps = remoteAddress;
    dtFrame.priority = 7;
    dtFrame.dlc = 8;

    currentPacket++;
    count--;

    if(count == 0) timer.stop();

    emit frameToSend(dtFrame);

    if(count == 0) ctsTimer.start(1250);
}

void transportProtocol::abort(int reason)
{


}

void transportProtocol::processFrame(canFrame frame)
{
    // TP.CM
    if(frame.pgn == 60416 && frame.ps == *ownAddress)
    {
        // TP.CM_RTS
        if((uchar)frame.data[0] == 16 && mode == TP_RECEIVE)
        {
            //qDebug() << "RTS: " << connected << frame.sa << remoteAddress;
            int tPGN = (uchar)frame.data[5] | ((uchar)frame.data[6] << 8) | ((uchar)frame.data[7] << 16);
            if( connected )
            {
                if(frame.sa != remoteAddress)
                {
                    // Abort Reason 1
                    // Unless we have multi instances of this class to handle reads, then we can just pass it off to another?

                    canFrame abortFrame;
                    abortFrame.sa = *ownAddress;
                    abortFrame.pf = 236;
                    abortFrame.ps = frame.sa;
                    abortFrame.priority = 7;
                    abortFrame.dlc = 8;
                    abortFrame.data[0] = 255;  // CTS
                    abortFrame.data[1] = 1;   // Reason (Reject connection, can't handle another)
                    abortFrame.data[2] = 0xFF;   // Resvd
                    abortFrame.data[3] = 0xFF;    // Resvd
                    abortFrame.data[4] = 0xFF;    // Resvd
                    abortFrame.data[5] = pgn & 0xFF;
                    abortFrame.data[6] = (pgn & 0xFF00 ) >> 8;
                    abortFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
                    emit frameToSend(abortFrame);
                    //qDebug() << "TP Rejected Incoming Connection Request";
                    return;
                }
                if(frame.sa == remoteAddress && tPGN != pgn)
                {
                    // Do we abort in this case? probably? 11783 part 3 isn't clear on this.
                    return;
                }
            }



            connected = true;
            receivedBytes = 0;
            totalBytes = ((uchar)frame.data[1] | ((uchar)frame.data[2] << 8));
            totalPackets = (uchar)frame.data[3];
            maxSend = (uchar)frame.data[4];
            pgn = (uchar)frame.data[5] | ((uchar)frame.data[6] << 8) | ((uchar)frame.data[7] << 16);

            count = 0;
            active = true;

            remoteAddress = frame.sa;

            currentPacket = 0;

            data.reserve(totalBytes);


            int packetsLeft = totalPackets - currentPacket;

            requestedCount = maxSend;

            if(packetsLeft < requestedCount) requestedCount = packetsLeft;

            //qDebug() << "TP.CM_RTS" << totalBytes << totalPackets << maxSend << pgn;

            // Now send a CTS
            canFrame ctsFrame;
            ctsFrame.sa = *ownAddress;
            ctsFrame.pf = 236;
            ctsFrame.ps = remoteAddress;
            ctsFrame.priority = 7;
            ctsFrame.dlc = 8;
            ctsFrame.data[0] = 17;  // CTS
            ctsFrame.data[1] = requestedCount;   // 2 Packets
            ctsFrame.data[2] = 1;   // Next packet is #1
            ctsFrame.data[3] = 0xFF;    // Resvd
            ctsFrame.data[4] = 0xFF;    // Resvd
            ctsFrame.data[5] = pgn & 0xFF;
            ctsFrame.data[6] = (pgn & 0xFF00 ) >> 8;
            ctsFrame.data[7] = (pgn & 0xFF0000 ) >> 16;


            //qDebug() << "TP.CM_CTS" << requestedCount << 1 << ((uchar)ctsFrame.data[5] | ((uchar)ctsFrame.data[6] << 8) | ((uchar)ctsFrame.data[7] << 16));

            count = 0;

            emit frameToSend(ctsFrame);

            ctsTimer.start(1250);
        }

        // TP.CM_CTS
        if((uchar)frame.data[0] == 17 && mode == TP_SEND && active && frame.sa == remoteAddress)
        {
            ctsTimer.stop();

            // Check if PGN matches the one in RTS?

            connected = true;
            currentPacket = (uchar)frame.data[2];  // Packet to start at
            count = (uchar)frame.data[1];  // Number of packets that can be sent
            if(count > 0)
            {
                onHold = false;
                timer.start(10);
            }
            else
            {
                onHold = true;
                ctsTimer.start(1050);
            }
        }

        // TP_CM_EndofMsgACK
        if((uchar)frame.data[0] == 19 && mode == TP_SEND)
        {
            ctsTimer.stop();
            timer.stop();
            count = 0;
            active = false;
            connected = false;
            emit sent(true);
        }

        // TP.Conn_Abort
        if((uchar)frame.data[0] == 255)
        {
            connected = false;
            active = false;
            timer.stop();
            ctsTimer.stop();
            count = 0;
            //qDebug() << "TP ABORT";
        }
    }

    // TP.DT
    if(frame.pgn == 60160 && mode == TP_RECEIVE && frame.sa == remoteAddress && frame.ps == *ownAddress)
    {
        if(connected)
        {
            ctsTimer.stop();
            //qDebug() << "TP.DT" << (uchar)frame.data[0];

            int remain = totalBytes - receivedBytes;
           // qDebug() << "Remain" << remain;
            if(remain > 7) remain = 7;
            for(int i=0;i<remain;i++)
            {
                data[ (((uchar)frame.data[0]-1)*7) + i ] = (uchar)frame.data[i+1];
                receivedBytes++;
            }
            if((uchar)frame.data[0] > currentPacket) currentPacket = (uchar)frame.data[0];
            count++;
            if(count == requestedCount)
            {   // next CTS or End of message ack

                if(currentPacket == totalPackets)
                {   // Done
                    canFrame ackFrame;

                    ackFrame.sa = *ownAddress;

                    ackFrame.pf = 236;
                    ackFrame.ps = remoteAddress;
                    ackFrame.priority = 7;
                    ackFrame.dlc = 8;
                    ackFrame.data[0] = 19;
                    ackFrame.data[1] = receivedBytes & 0xFF;
                    ackFrame.data[2] = (receivedBytes & 0xFF00) >> 8;
                    ackFrame.data[3] = currentPacket;
                    ackFrame.data[4] = 0xFF;    // Resvd
                    ackFrame.data[5] = pgn & 0xFF;
                    ackFrame.data[6] = (pgn & 0xFF00 ) >> 8;
                    ackFrame.data[7] = (pgn & 0xFF0000 ) >> 16;
                    active = false;
                    connected = false;

                    //qDebug() << "TP.CM_EomACK" << receivedBytes << currentPacket << (ackFrame.data[5] | (ackFrame.data[6] << 8) | (ackFrame.data[7] << 16));
                   // for(int i=0;i<tp.data.count();i++)
                   // {
                   //     ui->textBrowser->append(QString("%1").arg(tp.data[i]));
                   // }
                    emit frameToSend(ackFrame);

                    // Now process large message
                    emit received(*this);
                    return;
                }
                else
                {   // Next CTS
                    // If we need to rerequest something from prev cts, then we need to decrease receivedBytes count
                    // by that much

                    // Assuming for now all data from prev cts was valid and no resend needed
                    // How many packets left?
                    int packetsLeft = totalPackets - currentPacket;

                    requestedCount = maxSend;

                    if(packetsLeft < requestedCount) requestedCount = packetsLeft;

                    canFrame ctsFrame;
                    ctsFrame.sa = *ownAddress;
                    ctsFrame.pf = 236;
                    ctsFrame.ps = remoteAddress;
                    ctsFrame.priority = 7;
                    ctsFrame.dlc = 8;
                    ctsFrame.data[0] = 17;  // CTS
                    ctsFrame.data[1] = requestedCount;   // 2 Packets
                    ctsFrame.data[2] = currentPacket+1;   // Next packet is #1
                    ctsFrame.data[3] = 0xFF;    // Resvd
                    ctsFrame.data[4] = 0xFF;    // Resvd
                    ctsFrame.data[5] = pgn & 0xFF;
                    ctsFrame.data[6] = (pgn & 0xFF00 ) >> 8;
                    ctsFrame.data[7] = (pgn & 0xFF0000 ) >> 16;

                    //qDebug() << "TP.CM_CTS" << requestedCount << currentPacket+1 << ((uchar)ctsFrame.data[5] | ((uchar)ctsFrame.data[6] << 8) | ((uchar)ctsFrame.data[7] << 16));

                    count = 0;

                    emit frameToSend(ctsFrame);
                    ctsTimer.start(1250);
                    return;
                }

            }
            ctsTimer.start(750);
        }
    }
}
