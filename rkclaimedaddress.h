#ifndef RKCLAIMEDADDRESS_H
#define RKCLAIMEDADDRESS_H

#include <QObject>

#include <rkname.h>

class rkClaimedAddress
{
public:
    rkClaimedAddress();
    quint8 address;
    //quint64 name;
    rkNAME name;

    bool operator==(const rkClaimedAddress &that) const
    {
        return this->address == that.address;
    }
};

#endif // RKCLAIMEDADDRESS_H
