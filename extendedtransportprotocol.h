#ifndef EXTENDEDTRANSPORTPROTOCOL_H
#define EXTENDEDTRANSPORTPROTOCOL_H

#include <QObject>
#include <QByteArray>
#include <QTimer>

#include <canframe.h>

class extendedTransportProtocol : public QObject
{
    Q_OBJECT
public:
    extendedTransportProtocol(quint8 *a, int mode=false);

    quint8 *ownAddress;
    int remoteAddress;
    bool active;
    bool connected;

    bool txMode;

    int pgn;
    int totalBytes;
    QByteArray data;
    int currentPacket;
    int totalPackets;
    int lastPacketSize;
    int requestedCount;
    int count;
    bool haveDPO;
    int dpo;
    bool onHold;
    int maxSend;
private:
    QTimer sendTimer;
    QTimer timer;   // Timeout timer
    int percent;
signals:
    void frameToSend(canFrame);
    void received(extendedTransportProtocol &);
    void pct(int);
    void sendNext();
    void statusMessage(QString);

    void sent(bool);
private slots:
    void sendTimerTimeout();
    void proccessTimeout();
public slots:
    void processMessage(canFrame frame);
    void send(int dest, int s_pgn, const QByteArray &s_data);
};

#endif // EXTENDEDTRANSPORTPROTOCOL_H
