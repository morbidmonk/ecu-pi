#ifndef VT_COMMANDS_H
#define VT_COMMANDS_H

#include <QByteArray>

class VT_Commands
{
public:
    VT_Commands();

    static QByteArray HideShowObject(quint16 id, quint8 show);
    static QByteArray ChangeNumericValue(quint16 id, quint32 value);
    static QByteArray ChangeStringValue(quint16 id, QByteArray value);
    static QByteArray ChangeAttribute(quint16 id, quint8 aid, quint32 value);
};

#endif // VT_COMMANDS_H
