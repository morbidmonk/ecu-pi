#include <QCoreApplication>
#include <QTextStream>

#include <rknativeeventfilter.h>

#include <libusb-1.0/libusb.h>
#include <ecu.h>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTextStream out(stdout);

    out << "CANbus ECU Test" << endl;

    ecu e;

   // a.installNativeEventFilter(&e.winFilter);

    return a.exec();
}
