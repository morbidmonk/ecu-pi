#include "rknativeeventfilter.h"

#include <QtDebug>

rkNativeEventFilter::rkNativeEventFilter()
{

}

bool rkNativeEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, long *)
{
    qDebug() << eventType;
    if(eventType == "windows_generic_MSG")
    {
#ifdef _WIN32
    MSG *m;
    m = (MSG*)message;
    if(m->message == WM_DEVICECHANGE)
    {

        if(m->wParam == DBT_DEVNODES_CHANGED)
        {
            qDebug() << "DEV CHANGE";
        }
    }
#endif
    return false;
    }
}
