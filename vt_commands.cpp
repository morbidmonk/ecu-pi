#include "vt_commands.h"

#include <QDataStream>

VT_Commands::VT_Commands()
{

}

QByteArray VT_Commands::ChangeNumericValue(quint16 id, quint32 value)
{
    QByteArray t;
    t.resize(8);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)168 << id << (quint8)0xFF << value;
    return t;
}

QByteArray VT_Commands::ChangeStringValue(quint16 id, QByteArray value)
{
    QByteArray t;
    t.resize(5);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)179 << id << (quint16)value.length();

    t+=value;
    return t;
}

QByteArray VT_Commands::HideShowObject(quint16 id, quint8 show)
{
    QByteArray t;
    t.resize(8);
    t.fill(0xFF);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)160 << id << show;
    return t;
}

QByteArray VT_Commands::ChangeAttribute(quint16 id, quint8 aid, quint32 value)
{
    QByteArray t;
    t.resize(8);
    t.fill(0xFF);
    QDataStream ds(&t, QIODevice::WriteOnly);
    ds.setByteOrder((QDataStream::LittleEndian));

    ds << (quint8)175 << id << aid << value;
    return t;
}
