#include "rkname.h"

rkNAME::rkNAME()
{
    setNAME(0);
}

void rkNAME::setNAME(quint64 v)
{
    name = v;

    // Self Config
    quint64 t = (name >> 63) & 1;
    if(t) selfConfig = 1;
    else selfConfig = 0;
    // Manufactuer
    t = (name >> 21) & 0b11111111111;
    manufacturer = (int)t;
    // Industry
    t = (name >> 60) & 0b111;
    industry = (int)t;
    // Device Class
    t = (name >> 49) & 0b1111111;
    devClass = (int)t;
    // Device Function
    t = (name >> 40) & 0b11111111;
    devFunction = (int)t;
    // class Instance
    t = (name >> 56) & 0b1111;
    devClassInstance = (int)t;
    // Function Instance
    t = (name >> 35) & 0b11111;
    devFunctionInstance = (int)t;
    // Identity / Serial
    t = name & 0b111111111111111111111;
    identity = (int)t;

    // ECU Instance

    t = (name >> 32) & 0b111;
    ECUInstance = (int)t;
}

void rkNAME::setSelfConfig(quint32 v)
{
    quint64 temp = name;
    quint64 mask = 0xEFFFFFFFFFFFFFFF;
    temp &= mask;
    if(v>1) v = 1;
    selfConfig = v;
    mask = v;
    mask = mask << 63;
    name = temp | mask;
}
void rkNAME::setIndustry(quint32 v)
{
    industry = v;
    quint64 temp = name;
    quint64 mask = 0x8FFFFFFFFFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 60;
    name = temp | mask;
}
void rkNAME::setDevClassInstance(quint32 v)
{
    devClassInstance = v;
    quint64 temp = name;
    quint64 mask = 0xF0FFFFFFFFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 56;
    name = temp | mask;
}
void rkNAME::setDevClass(quint32 v)
{
    devClass = v;
    quint64 temp = name;
    quint64 mask = 0xFF00FFFFFFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 49;
    name = temp | mask;
}
void rkNAME::setDevFunction(quint32 v)
{
    devFunction = v;
    quint64 temp = name;
    quint64 mask = 0xFFFF00FFFFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 40;
    name = temp | mask;
}
void rkNAME::setDevFunctionInstance(quint32 v)
{
    devFunctionInstance = v;
    quint64 temp = name;
    quint64 mask = 0xFFFFFF07FFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 35;
    name = temp | mask;
}
void rkNAME::setECUInstance(quint32 v)
{
    ECUInstance = v;
    quint64 temp = name;
    quint64 mask = 0xFFFFFFF8FFFFFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 32;
    name = temp | mask;
}
void rkNAME::setManufacturer(quint32 v)
{
    manufacturer = v;
    quint64 temp = name;
    quint64 mask = 0xFFFFFFFF001FFFFF;
    temp &= mask;
    mask = v;
    mask = mask << 21;
    name = temp | mask;
}
void rkNAME::setIdentity(quint32 v)
{
    identity = v;
    quint64 temp = name;
    quint64 mask = 0xFFFFFFFFFFE00000;
    temp &= mask;
    mask = v;
    name = temp | mask;
}
