#ifndef USB_DEVICE_H
#define USB_DEVICE_H

#include <QThread>
#include <QString>
#include <QFile>
#include <QTextStream>
//#include <QInputDialog>
#include <QList>

#ifdef _WIN32
    #include "windows.h"
    #include "Dbt.h"
#endif

#include "math.h"
#include <libusb-1.0/libusb.h>
#include <QDebug>

#include <canframe.h>



class CUSBDevice : public QThread
{
	Q_OBJECT
public:
    CUSBDevice(unsigned int vendor, unsigned int product, unsigned int interface);
	~CUSBDevice();
	bool	isPresent();




    bool loopback;

protected:
	void run();
private:

    unsigned int vendorID;
    unsigned int productID;
    unsigned int interfaceNumber;
	bool	AttachedState;
	bool	AttachedButBroken;
    QList<int> data;
    libusb_device_handle *handle;

    static int LIBUSB_CALL hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data);

    char id;

    libusb_hotplug_callback_handle cb_handle;

public slots:
    bool	checkAttached();
    void    writeCAN(canFrame frame);

    void    loopbackCAN(bool lpb);
    void    requestStatus();

signals:
    void readyRead(QString);
    void dataReady(int, int);
    void status(bool);
    //void ISO(dataPack);

    void readCAN(canFrame);

	void	info(QString);
	void	attached();
	void	removed();
};

#endif
