#include "usbDevice.h"

#include <QDebug>

CUSBDevice::CUSBDevice(unsigned int vendor, unsigned int product, unsigned int interface)
{
    id = 0;
    AttachedState = false;
    AttachedButBroken = false;

    vendorID = vendor;
    productID = product;
    interfaceNumber = interface;

    qRegisterMetaType<canFrame>("canFrame");

    handle = NULL;
    libusb_init(NULL);

    qDebug() << "USB CTOR";

    if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
    {
        int rc = libusb_hotplug_register_callback(NULL, (libusb_hotplug_event) (LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT), (libusb_hotplug_flag)(0), vendor, product, LIBUSB_HOTPLUG_MATCH_ANY, &hotplug_callback, this, &cb_handle);
        qDebug() << "HOTPLUG: " << QString((char*)libusb_error_name(rc));
    }
}

CUSBDevice::~CUSBDevice()
{
    if(handle != NULL) libusb_close(handle);
    libusb_exit(NULL);
}

bool CUSBDevice::checkAttached()
{
    bool found = false;
    libusb_device **list;

    int n = libusb_get_device_list(NULL, &list);
    libusb_device *dev;
    libusb_device_descriptor desc;

    for(int i=0;i<n;i++)
    {
        dev = list[i];
        libusb_get_device_descriptor(dev, &desc);

        if( desc.idVendor == vendorID && desc.idProduct == productID)
        {
            found = true;
            break;
        }

    }


    if(!AttachedState && found)
    {   // Found and need to open
        int r = libusb_open(dev, &handle);

        if(r==0)
        {   // Have it
            AttachedState = true;


            libusb_set_auto_detach_kernel_driver(handle, 1);
            libusb_claim_interface(handle, interfaceNumber);

            unsigned char buff[8192];
            libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_IN | 2, buff, 4096, 0, 10000);    // Give the buffer a good flush, removes odd stuff partic in VM


            emit attached();
           // qDebug() << "USB Successfully Opened";
        }
        else
        {
            qDebug() << "USB Failed to Open";
        }
    }

    if(AttachedState && !found)
    {   // Device Removed
        AttachedState = false;
        libusb_close(handle);
        handle = NULL;
        emit removed();
    }


    libusb_free_device_list(list,1);
    return AttachedState;


}

bool CUSBDevice::isPresent()
{
    bool found = false;
    libusb_device **list;

    int n = libusb_get_device_list(NULL, &list);
    libusb_device *dev;
    libusb_device_descriptor desc;

    for(int i=0;i<n;i++)
    {
        dev = list[i];
        libusb_get_device_descriptor(dev, &desc);

        if( desc.idVendor == vendorID && desc.idProduct == productID)
        {
            found = true;
        }

    }

    libusb_free_device_list(list,1);
    return found;
}


void CUSBDevice::run()
{

    int bytesRead = 0;
    unsigned char buff[8192];

    buff[0]=0;
    bool done = false;
    // Sit and read from device, emitting signal when data packet arrives
    while(!done)
    {
        if(AttachedState == true)
        {

             buff[0]=0;
            int suc = libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_IN | 2, buff, 64, &bytesRead, 10000);

            if( suc == LIBUSB_SUCCESS)
            {
                int count = (unsigned char) buff[0];

               // qDebug() << bytesRead;
               // if(count >4)qDebug() << "canFrames in USB Message: " << count;
                //if(count==0) qDebug() <<"0 frames";
                if(count > 4) count = 0;

                //dataPack d;


                //d.count = (unsigned char) buff[0];

                //d.st = (unsigned char) buff[62];
;
                unsigned char mode = (unsigned char) buff[62];
                mode = mode >> 5;
                mode &= 0x03;

                unsigned char modeValid = (unsigned char) buff[63];

                if(modeValid ==1) qDebug() << "Mode Valid" << mode;

                //qDebug() << mode;

                if(mode == 0 && modeValid == 1)
                {
                    //qDebug() << "Normal Mode";
                    emit status(false);

                }
                if(mode == 2 && modeValid == 1)
                {
                    //qDebug() << "Loopback Mode";
                    emit status(true);
                }


                int i = 1;

                unsigned char t;

                canFrame frame;

                for(int m=0;m<count;m++)
                {
                    // STD ID H (P3:P1 , EDP, DP, PF8:PF6

                    frame.priority = (unsigned char)buff[i];
                    frame.priority &= 0xE0;
                    frame.priority = frame.priority >> 5;

                    frame.pf = (unsigned char)buff[i];
                    frame.pf &= 0x07;
                    frame.pf = frame.pf << 5;

                    t = buff[i];

                    frame.edp = (t & 0x10);
                    frame.dp  = (t & 0x08);

                    i++;

                    // STD ID L ( PF5:PF3 , SRR, EXID, - , PF2:PF1

                    t = buff[i];

                    frame.ide = (t & 0x08);

                    t &= 0xE0;
                    t = t >> 3;
                    frame.pf |= (unsigned char)t;

                    t = buff[i];
                    t &= 0x03;
                    frame.pf |= (unsigned char)t;

                    i++;

                    // EXT ID H (PS8:PS1)

                    frame.ps = (unsigned char)buff[i++];

                    // EXT ID L (SA8:SA1)

                    frame.sa = (unsigned char)buff[i++];

                    // DLC ( - , RTR(1) , - , - , DLC3:DLC0 )

                    frame.dlc = (unsigned char)buff[i++];
                    frame.dlc &= 0x0F;

                    for(int n=0;n<8;n++)
                    {
                        frame.data[n] = (unsigned char)buff[i++];
                    }

                    int pgn = 0;

                    if(frame.edp) pgn |= 2;
                    if(frame.dp) pgn |=1;
                    pgn = pgn << 8;

                    pgn |= frame.pf;
                    pgn = pgn << 8;

                    if(frame.pf >= 240) pgn |= frame.ps;

                    frame.pgn = pgn;

                    emit readCAN(frame);
                }
                //emit ISO(d);
                //emit dataReady(V, C);
                // qDebug() << "USB Read";

                if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
                {
                    timeval tv;
                    tv.tv_sec = 0;
                    tv.tv_usec = 0;
                    libusb_handle_events_timeout_completed(NULL,&tv,NULL);
                }
            }
            else
            {
                sleep(1);

                if(AttachedState == true)
                {
                    emit info("Read Failed");
                    emit info(QString((char*)libusb_error_name(suc)));
                    qDebug() << "USB Read Failed: " << QString((char*)libusb_error_name(suc));
                }

                if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
                {
                    timeval tv;
                    tv.tv_sec = 0;
                    tv.tv_usec = 0;
                    libusb_handle_events_timeout_completed(NULL,&tv,NULL);
                };
            }
        }
        else
        {

            if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
            {
                timeval tv;
                tv.tv_sec = 0;
                tv.tv_usec = 0;
                libusb_handle_events_timeout_completed(NULL,&tv,NULL);
            }
            sleep(1);
        }

    }
}


void CUSBDevice::requestStatus()
{
    int bytesWritten = 0;
    unsigned char data[80];

     data[0] = 0x02;


    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
        }
        else
        {
            qDebug() << "Status Requested";
        }

    }
}

void CUSBDevice::loopbackCAN(bool lpb)
{
    int bytesWritten = 0;
    unsigned char data[80];
    if(lpb)
    {
        data[0] = 0x05; // Loopback Mode
    }
    else
    {
        data[0] = 0x04; // Normal Mode
    }

    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
        }
        else
        {
            qDebug() << "Loopback Mode set to " << lpb;
        }

    }
}


void CUSBDevice::writeCAN(canFrame frame)
{
  //  qDebug() << "WriteCAN: PGN: " << frame.pgn << " SA: " << frame.sa << " Attached: " << AttachedState;
    int bytesWritten = 0;
    unsigned char data[80];

    data[0] = 0x01; // Write
    data[1] = (unsigned char)(frame.priority << 5);
    if(frame.edp) data[1] |= 0x10;
    if(frame.dp) data[1] |= 0x08;
    data[1] |= (unsigned char) (frame.pf >> 5);


    unsigned char t1,t2;
    data[2] = (unsigned char)(frame.pf & 0x03);
    data[2] |= (unsigned char)((frame.pf << 3) & 0xE0);
    if(frame.ide) data[2] |= 0x08;

    data[3] = (unsigned char)frame.ps;
    data[4] = (unsigned char)frame.sa;
    data[5] = (unsigned char)frame.dlc;

    for(int i=0 ; i< 8 ; i++)
    {
        data[i+6] = (unsigned char)frame.data[i];
    }



    if(loopback)
    {

        int pgn = 0;

        if(frame.edp) pgn |= 2;
        if(frame.dp) pgn |=1;
        pgn = pgn << 8;

        pgn |= frame.pf;
        pgn = pgn << 8;

        if(frame.pf >= 240) pgn |= frame.ps;

        frame.pgn = pgn;


        emit readCAN(frame);
        return;
    }

    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
            return;
        }
        //else
        //{
         //   qDebug() << "OK";

          //  qDebug() << "WriteCAN: PGN: " << frame.pgn << " SA: " << frame.sa << " Attached: " << AttachedState << "Bytes: " << bytesWritten;

        //}

    }



}



 int  CUSBDevice::hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
     (void)ctx;
     (void)dev;
     (void)event;
     (void)user_data;
     //qDebug() << "Hotplug";
     static_cast<CUSBDevice*>(user_data)->checkAttached();
     return 0;
}

