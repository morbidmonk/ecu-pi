#include "canframe.h"
#include <algorithm>

canFrame::canFrame()
{
    edp = false;
    dp = false;
    ide = true;
    dlc = 0;
    data.fill(0xFF,8);
}

void canFrame::setPGN(int p)
{
    edp = false;
    dp = false;
    if(p & 0x010000) dp = true;
    if(p & 0x020000) edp = true;
    pf = (p >> 8) & 0xFF;
    ps = p & 0xFF;
}
