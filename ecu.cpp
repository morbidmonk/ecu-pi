#include "ecu.h"
#include <QTextStream>
#include <QDataStream>
#include <QFile>

ecu::ecu(QObject *parent) : QObject(parent)
{
    addressManager.name.setSelfConfig(1);
    addressManager.name.setIndustry(2);  // Agri
    addressManager.name.setDevClassInstance(0);
    addressManager.name.setDevClass(6);  // Sprayer
    addressManager.name.setDevFunction(132); // Sprayers Machine Control
    addressManager.name.setDevFunctionInstance(0);
    addressManager.name.setECUInstance(0);
    addressManager.name.setManufacturer(949); // Skoda  (unlikley to encounter a skoda ecu on a tractor!)
    addressManager.name.setIdentity(67545);

    usb = new CUSBDevice(0x4d8, 0x04b1, 0x01);

    connect(usb, SIGNAL(attached()), this, SLOT(usb_attached()));
    connect(usb, SIGNAL(removed()), this, SLOT(usb_removed()));
    connect(usb, SIGNAL(status(bool)), this, SLOT(loopbackStatus(bool)));
    connect(usb,SIGNAL(readCAN(canFrame)),this,SLOT(canFrameRX(canFrame)));

    connect(&addressManager, qOverload<bool>(&rkAddressManager::claimed), this, &ecu::addressClaimed);
    connect(&addressManager, qOverload<canFrame>(&rkAddressManager::sendCANframe), usb, &CUSBDevice::writeCAN);

    connect(&vtManager, qOverload<canFrame>(&rkVtManager::sendCANframe), usb, &CUSBDevice::writeCAN);

    connect(&mixCalculator, qOverload<QByteArray>(&mixCalc::cmdOut), &vtManager, &rkVtManager::queue_cmd);

    connect(&vtManager, &rkVtManager::poolSent, &mixCalculator, &mixCalc::load);

    connect(&vtManager, qOverload<QByteArray>(&rkVtManager::cmdReceived), &mixCalculator, &mixCalc::processCommand);

    connect(&vtManager, qOverload<QByteArray>(&rkVtManager::cmdReceived), &auxFuncManager, &rkAuxFunctionManager::processCommand);

    connect(&vtManager, &rkVtManager::poolSent,this,&ecu::poolSent);

    connect(&auxFuncManager, qOverload<canFrame>(&rkAuxFunctionManager::sendCANframe), usb, &CUSBDevice::writeCAN);

    connect(&vtManager, &rkVtManager::vtLost, &auxFuncManager, &rkAuxFunctionManager::vtLost);

    usb->loopback = false;

    usb->checkAttached();
    usb->start();
}

void ecu::poolSent()
{
     auxFuncManager.doPrefAssignment();
}

void ecu::usb_attached()
{
   QTextStream out(stdout);
   out << "ECU Board Attached" << Qt::endl;
   usb->requestStatus();
   addressManager.claimAddress(129);   // Claim address, preferred 129
}

void ecu::usb_removed()
{
   QTextStream out(stdout);
   out << "ECU Board Removed" << Qt::endl;
}

void ecu::loopbackStatus(bool v)
{
    QTextStream out(stdout);
    out << "ECU Board Loopback: " << v << Qt::endl;
}

void ecu::canFrameRX(canFrame f)
{
    QTextStream out(stdout);
   // out << "frame rx: sa=" << f.sa << " pgn=" << f.pgn << endl;

    if(vtManager.getState() == 1000)
    {
        auxFuncManager.processCANframe(f);
    }

    addressManager.processCANframe(f);  // Maybe do this only if pgn's are relevant?
    if(vtManager.processCANframe(f))
    {
        // Its a VT to ECU Command (58880) and is from our VT to us
       // mixCalculator.processCommand(f.data);

        // Do button tests here?
        QDataStream ds(f.data);
        ds.setByteOrder(QDataStream::LittleEndian);

        quint8 cmd;
        quint16 id;

        ds >> cmd;

        if(cmd == 1)
        {
            quint8 state;
            ds >> state >> id;

            if(id == 2001 && state == 1)
            {
                // Delete Stored Pools
                QByteArray o;
                o.fill(' ', 8);
                QDataStream os(&o, QIODevice::WriteOnly);
                os.setByteOrder(QDataStream::LittleEndian);

                os << (quint8)210;
               //o[7] = (quint8)'*';

                vtManager.queue_cmd(o);
            }

            if(id == 2002 && state == 1)
            {
                // Send working set master message

               /* canFrame tx;
                tx.pf = 254;
                tx.ps = 13;
                tx.sa = addressManager.address;
                tx.priority = 7;
                tx.dlc = 8;
                tx.data[0] = 1; // Just us in the set
                usb->writeCAN(tx);*/

                // Temp reuse as a resend pref aux assignment message
                /*QByteArray t;

                t.resize(8);
                t.fill(0xFF,8);
                QDataStream ds(&t, QIODevice::WriteOnly);
                ds.setByteOrder((QDataStream::LittleEndian));

                ds << (quint8)34 << (quint8)0x00;

                vtManager.queue_cmd(t);*/

                // Temp re-reuse as a trigger to dump aux assigments to console
                QMapIterator<quint16, rkAuxAssignment*>i(auxFuncManager.assignments);
                qDebug() << "--- Aux Assignments -----------------------------";
                while(i.hasNext())
                {
                    i.next();
                    rkAuxAssignment *aux = i.value();
                    qDebug() << "Func:" << aux->funcId << "Input:" << aux->inputId << "Name:" << QString::number(aux->aiuName,16) << "MiC:" << aux->aiuModelIdCode << "Addr:" << aux->address;
                }

                qDebug() << "--- Pref Input Units -----------------------------";
                for(int n=0;n<auxFuncManager.prefUnits.count();n++)
                {
                    qDebug() << "Name:" << QString::number(auxFuncManager.prefUnits.at(n).name.name, 16) << "MiC:" << auxFuncManager.prefUnits.at(n).modelIdCode << "nFuncs:" << auxFuncManager.prefUnits.at(n).nFunctions;
                }

                qDebug() << "--- Pref Assignments -----------------------------";

                QMapIterator<quint16, rkPrefAuxAssignment>k(auxFuncManager.prefAssignments);
                while(k.hasNext())
                {
                    k.next();
                    qDebug() << "Func:" << k.value().funcId << "Input:" << k.value().inputId << "Name:" << QString::number(k.value().name.name,16) << "MiC:" << k.value().modelIdCode;
                }

                qDebug() << "--- End of Aux Assignments -----------------------";



            }
        }
    }



   /* if(f.pgn == 58880)
    {
        mixCalculator.processCommand(f.data);
    }*/

}

void ecu::addressClaimed(bool v)
{
    QTextStream out(stdout);
    if(v)
    {
        out << "Address Claimed: " << addressManager.address << Qt::endl;

        auxFuncManager.vt = &vtManager;

        vtManager.addressManager = &addressManager;
        vtManager.init(0);
    }
    else
    {
        out << "ERROR: Couldn't claim an address!" << Qt::endl;
    }
}
