#ifndef ECU_H
#define ECU_H

#include <QObject>
#include <usb/usbDevice.h>

#include <QVector>
#include <QTimer>

#include <rkclaimedaddress.h>
#include <rkaddressmanager.h>

#include <rkvtmanager.h>


#include <rknativeeventfilter.h>

#include <mixcalc.h>

#include <rkauxfunctionmanager.h>

class ecu : public QObject
{
    Q_OBJECT
public:
    explicit ecu(QObject *parent = nullptr);

    rkNativeEventFilter winFilter;
private:
    CUSBDevice *usb;

    rkAddressManager addressManager;

    rkVtManager vtManager;

    mixCalc mixCalculator;

    rkAuxFunctionManager auxFuncManager;

    QTimer timer;
private slots:
    void usb_attached();
    void usb_removed();
    void loopbackStatus(bool v);
    void canFrameRX(canFrame f);

    void addressClaimed(bool);

    void poolSent();
signals:

};

#endif // ECU_H
