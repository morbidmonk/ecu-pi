#ifndef RKVTMANAGER_H
#define RKVTMANAGER_H

#include <QObject>
#include <QTimer>

#include <QQueue>

#include <rkname.h>
#include <rkaddressmanager.h>
#include <canframe.h>

#include <transportprotocol.h>
#include <extendedtransportprotocol.h>

#include <vt_commands.h>

/*
 Init Proc

 - Init called with a vt instance number
 - check address table for VT
 - if found store address and wait a timeout for a VT status message (to 255 or us)
 - if not found monitor address claims for the VT (for the timeout time as well?)
 - when VT status received, send WS Master message, then WS Maint Message (with init bit set)
 - then send WS Maint every 1000ms (no init bit)
 - query VT capabilitys
 - emit init finished signal?
  */

class rkVtManager : public QObject
{
    Q_OBJECT
public:
    explicit rkVtManager(QObject *parent = nullptr);

    void init(quint8 vt=0);   // Init connection to vt instance #vt

    rkAddressManager *addressManager; // So we have access to address table to check for VT's

    rkNAME name;
    quint8  address;
    quint8 ourVersion;
    quint8 vtVersion;


    void cmd_ChangeNumericValue(quint16 id, quint32 v);
    void cmd_ChangeStringValue(quint16 id, QString v);

    QByteArray poolVersion;
    bool store;

    int getState()
    {
        return state;
    }

public slots:
    bool processCANframe(canFrame f);
    void sendObjectPool(QByteArray &pool);
    void queue_cmd(QByteArray cmd);

private:
    QTimer maintainanceTimer; // 1000ms , send wsm message
    QTimer statusTimer; // 3000ms, reset when vt status received, if reaches timeout then VT unexpected shutdown.

    int state;

    quint8 instance;

    bool WS_Initialized;

    transportProtocol *tp_out;
    extendedTransportProtocol *etp_out;

    transportProtocol *tp_in;

    QByteArray objectPool;
    int sendPoolState;

    QQueue<QByteArray> commandQueue;
private slots:
    void statusTimerTimeout();
    void maintainanceTimerTimeout();

    void etpSent(bool ok);

    void tpReceived(transportProtocol &tp);

    void processCommand(QByteArray &data);
signals:
    void sendCANframe(canFrame);
    void vtFound();
    void vtLost();

    void poolSent();

    void cmdReceived(QByteArray);
};

#endif // RKVTMANAGER_H
