#include "mixcalc.h"

#include <vt_commands.h>
#include <QDataStream>
#include <QFile>

#include <QDebug>

mixCalc::mixCalc()
{

    products[0].name = QString("Roundup Flex").toLocal8Bit();
    products[0].rate = 1.0;
    products[0].units = 0;

    for(int i=1; i<12;i++)
    {
        products[i].name = QString("Product %1").arg(i+1).toLocal8Bit();
        products[i].rate = 0;
        products[i].units = 0;
    }


    volRate = 180;;
    rearFillTo=1200;
    frontFillTo=0;
    frontRemain=0;
    rearRemain=0;
}

void mixCalc::relist()
{
    int row = 0;

    for(int i=0;i<12;i++) // i is product num
    {
        if(products[i].rate != 0)
        {
            // Show Row Container[row]
            // Set ptr to product[i]
            quint16 id = MC::rear_row_base + row;
            // Hide/Show Object id (show)
            quint16 ptrId = MC::rear_row_ptr_base + row;
            quint16 prodId = MC::rear_prod_out_base + i;

            emit cmdOut(VT_Commands::HideShowObject(id,1));
            emit cmdOut(VT_Commands::ChangeNumericValue(ptrId, prodId));

            id = MC::front_row_base + row;
            ptrId = MC::front_row_ptr_base + row;
            prodId = MC::front_prod_out_base + i;

            emit cmdOut(VT_Commands::HideShowObject(id,1));
            emit cmdOut(VT_Commands::ChangeNumericValue(ptrId, prodId));

            row++;
        }
    }

    // Any remaining rows..hide
    for(int i=row;i<12;i++)
    {
        quint16 id = MC::rear_row_base + row;
        // Hide/Show Object id (hide)
        emit cmdOut(VT_Commands::HideShowObject(id,0));

        id = MC::front_row_base + row;
        emit cmdOut(VT_Commands::HideShowObject(id,0));

        row++;
    }
}

void mixCalc::recalcFront()
{
    double water = frontFillTo - frontRemain;
    double ha = water/(double)volRate;
    for(int i=0;i<12;i++)
    {
        if(products[i].rate != 0)
        {
            double amount = products[i].rate * ha;

            quint32 v = amount / 0.001;

            quint16 id = 1161 + i;

            emit cmdOut(VT_Commands::ChangeNumericValue(id,v));

            if(products[i].units == 0)
            {   // Litres
                water -= amount;
            }
        }
    }
    quint32 h20 = water / 0.001;
    emit cmdOut(VT_Commands::ChangeNumericValue(1176, h20));

}

void mixCalc::recalc()
{
    double water = rearFillTo - rearRemain;

    double ha = water/(double)volRate;

    qDebug() << "ha =" << ha;

    for(int i=0;i<12;i++)
    {
        if(products[i].rate != 0)
        {
            double amount = products[i].rate * ha;

            quint32 v = amount / 0.001;

            quint16 id = 1125 + i;

            emit cmdOut(VT_Commands::ChangeNumericValue(id,v));

            if(products[i].units == 0)
            {   // Litres
                water -= amount;
            }
        }
    }
    quint32 h20 = water / 0.001;
    emit cmdOut(VT_Commands::ChangeNumericValue(1086, h20));
}

void mixCalc::processCommand(QByteArray v)
{
    // Looking for..
    // 5 - Change Numeric Value ( 5, id, 0xff, value)
    // 8 - Change String Value ( 8, id, nbytes, value)

    quint8 cmd;
    quint16 id;

    QDataStream ds(v);
    ds.setByteOrder(QDataStream::LittleEndian);

    ds >> cmd;

    if( cmd == 5)
    {
        ds >> id;
        ds.skipRawData(1);
        quint32 value;
        ds >> value;

        // Volume Rate? [1014]
        if(id == 1014)
        {
            volRate = value;
            recalc();
            recalcFront();
            save();
            return;
        }
        // Front Fill To
        if(id==1174)
        {
            frontFillTo = value;
            qDebug() << "Front Fill To =" << frontFillTo;
            recalcFront();
            save();
            return;
        }
        // Front In Tank
        if(id==1173)
        {
            frontRemain = value;
            recalcFront();
            save();
            return;
        }
        // Rear Fill To [1069]
        if(id==1070)
        {
            rearFillTo = value;
            qDebug() << "Rear Fill To =" << rearFillTo;
            recalc();
            save();
            return;
        }
        // Rear in Tank [1070]
        if(id==1069)
        {
            rearRemain = value;
            recalc();
            save();
            return;
        }
        // Product rate 1-12 [1041:1052]
        if(id >= 1041 && id <= 1052)
        {
            int idx = id - 1041;
            products[idx].rate = (double)value * 0.001;
            relist();
            recalc();
            recalcFront();
            save();
            return;
        }
        // Product units 1-12 [1053:1064]
        if(id >= 1053 && id <= 1064)
        {
            int idx = id - 1053;
            products[idx].units = (quint8)value;
            recalc();
            recalcFront();
            save();
            return;
        }
    }

    if( cmd == 8)
    {
        ds >> id;
        quint8 nBytes;
        QByteArray str;



        ds >> nBytes;



        char buff[128];

        ds.readRawData(buff,nBytes);

        buff[nBytes]=0x00;

        str = QByteArray(buff,nBytes);



        QString a(buff);

        qDebug() << "String Received: Id=" << id << "bytes=" << nBytes << "value=" << a << str;

        if(id >= 1101 && id <= 1112)
        {

            quint16 idx = id-1101;
            products[idx].name = str; //a.toLocal8Bit();

            qDebug() << "Index=" << idx << products[idx].name;
            save();
        }
    }
}

void mixCalc::save()
{
    QFile f("mixcalc.bin");
    f.open(QIODevice::WriteOnly);

    QDataStream ds(&f);
    ds.setByteOrder(QDataStream::LittleEndian);
    ds.setFloatingPointPrecision(QDataStream::DoublePrecision);

    ds << volRate << frontFillTo << frontRemain << rearFillTo << rearRemain;
    for(int i=0;i<12;i++)
    {
        ds << products[i].name << products[i].rate << products[i].units;
    }
    f.close();
}

void mixCalc::load()
{
    QFile f("mixcalc.bin");
    if(f.open(QIODevice::ReadOnly))
    {
        QDataStream ds(&f);
        ds.setByteOrder(QDataStream::LittleEndian);
        ds.setFloatingPointPrecision(QDataStream::DoublePrecision);

        ds >> volRate >> frontFillTo >> frontRemain >> rearFillTo >> rearRemain;
        for(int i=0;i<12;i++)
        {
            ds >> products[i].name >> products[i].rate >> products[i].units;
        }
        f.close();
    }

    // Now send all values to VT, then relist and recalc

    emit cmdOut(VT_Commands::ChangeNumericValue(1014, volRate));
    emit cmdOut(VT_Commands::ChangeNumericValue(1070, rearFillTo));
    emit cmdOut(VT_Commands::ChangeNumericValue(1069, rearRemain));

    emit cmdOut(VT_Commands::ChangeNumericValue(1174, frontFillTo));
    emit cmdOut(VT_Commands::ChangeNumericValue(1173, frontRemain));

    for(int i=0;i<12;i++)
    {
        emit cmdOut(VT_Commands::ChangeNumericValue(1041+i, products[i].rate/0.001));
        emit cmdOut(VT_Commands::ChangeNumericValue(1053+i, products[i].units/0.001));
        emit cmdOut(VT_Commands::ChangeStringValue(1101+i,products[i].name));
    }

    relist();
    recalc();
    recalcFront();
}
