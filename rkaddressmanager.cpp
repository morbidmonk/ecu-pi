#include "rkaddressmanager.h"

#include <QDataStream>
#include <QDebug>

rkAddressManager::rkAddressManager(QObject *parent) : QObject(parent)
{
    state = 0;
    curAddr = 254;
    address = 254;
    initAddr = 254;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, this, &rkAddressManager::timeout);
}

void rkAddressManager::claimAddress(quint8 ia)
{
    // Request Address Claimed & build an address table
    initAddr = ia;
    addressTable.clear();
    canFrame f;
    f.pgn = 59904;  // Request
    f.pf = 234;
    f.ps = 255;     // Global
    f.sa = curAddr;     // Null Address
    f.dlc = 3;
    f.priority = 6;
    f.data[0] = (char)(60928 & 0xFF);
    f.data[1] = (char)((60928 >> 8) & 0xFF);
    f.data[2] = (char)((60928 >> 16) & 0xFF);
    emit sendCANframe(f);

    // Start Timer
    timer.start(1250);  // Std says 250ms + RTxD but we will go for 1250, maybe adding RTxD at some point
    state = 1;

   // emit sendCANframe(f);
}

void rkAddressManager::processCANframe(canFrame f)
{
    if(f.pgn == 59904)  // Request
    {
        int requestedPGN = (uchar)f.data[0] | ((uchar)f.data[1] << 8) | ((uchar)f.data[2] << 16);
        if(state == 3 && (f.ps == 255 || f.ps == address) ) // Either Global or sent to us
        {
            if(requestedPGN == 60928)   // Request Address Claimed
            {
                canFrame f;
                f.pgn = 60928;
                f.pf = 238;
                f.ps = 255;     // Global
                f.sa = initAddr;
                f.dlc = 8;
                f.priority = 6;

                QDataStream ds(&f.data, QIODevice::WriteOnly);
                ds.setByteOrder(QDataStream::LittleEndian);
                ds << this->name.name;
                emit sendCANframe(f);
                return;
            }
        }
    }
    if(f.pgn == 60928) // Address Claimed
    {
        rkClaimedAddress ta;
        ta.address = f.sa;
        QDataStream ds(f.data);
        ds.setByteOrder(QDataStream::LittleEndian);
        quint64 tn;
        ds >> tn;
        ta.name.setNAME(tn);

        if(state == 2 || state == 3)
        {
            if(f.sa == initAddr)
            {   // Conflict, to be fair i'm not sure this should happen in state 3
                // Compare names if we have higher priority send an address claimed back
                // else find a new address
                if(name.name < tn) // We have priority
                {
                    canFrame f;
                    f.pgn = 60928;
                    f.pf = 238;
                    f.ps = 255;     // Global
                    f.sa = initAddr;
                    f.dlc = 8;
                    f.priority = 6;

                    QDataStream ds(&f.data, QIODevice::WriteOnly);
                    ds.setByteOrder(QDataStream::LittleEndian);
                    ds << this->name.name;
                    emit sendCANframe(f);
                    return;
                }
                else
                {
                    state = 0;
                    claimAddress(128);
                    return;
                }

            }
        }

        // Add to address table
        bool done = false;
        for(int i=0;i<addressTable.size();i++)
        {
            if(addressTable.at(i).address == ta.address)
            {
                addressTable.replace(i, ta);
                done = true;
                break;
            }
        }
        if(!done) addressTable.append(ta);
    }
}

void rkAddressManager::timeout()
{
    if(state == 1) // Building Address Table
    {
        qDebug() << "Address Table Built: Devices=" << addressTable.size();


        rkClaimedAddress ta;
        ta.address = initAddr;
        // Find first free address, starting with pref
        if(addressTable.contains(ta))
        {
            for(int i=128; i<249;i++)
            {
                ta.address = i;
                if( !addressTable.contains(ta) )
                {
                    break;
                }
            }
        }
        // Now if ta == 248 we didn't find a free address (unlikley)
        // else address in ta is the address to claim
        if( ta.address != 248 )
        {
            initAddr = ta.address;
            // Claim Address
            // Then wait 250ms, if no counter claims in that time we are good to go
            canFrame f;
            f.pgn = 60928;  // Request
            f.pf = 238;
            f.ps = 255;     // Global
            f.sa = ta.address;     // Null Address
            f.dlc = 8;
            f.priority = 6;

            QDataStream ds(&f.data, QIODevice::WriteOnly);
            ds.setByteOrder(QDataStream::LittleEndian);
            ds << this->name.name;
            emit sendCANframe(f);
            timer.start(250);
            state = 2;
        }
        else
        {
            state = -1;
        }
        return;
    }

    if(state == 2) // Waiting for any counter claim
    {
        // If we've made it here we are good to use this address
        address = initAddr;
        state = 3;
        emit claimed(true);
        return;
    }
}

bool rkAddressManager::haveAddress()
{
    if(state == 3) return true;
    return false;
}
