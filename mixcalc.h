#ifndef MIXCALC_H
#define MIXCALC_H

#include <QObject>
#include <QVector>

namespace MC {
    const int rear_row_base = 1072;
    const int rear_prod_out_base = 1113;
    const int rear_row_ptr_base = 1089;

    const int front_row_base = 1177;
    const int front_prod_out_base = 1149;
    const int front_row_ptr_base = 1189;
}

class product
{
public:
    product()
    {
        name.resize(20);
        name.fill(' ',20);
    }
    QByteArray name;
    double rate;
    quint8 units;
};

class mixCalc : public QObject
{
    Q_OBJECT
public:
    mixCalc();

    product products[12];

    quint32 volRate;
    quint32 rearFillTo;
    quint32 frontFillTo;
    quint32 frontRemain;
    quint32 rearRemain;

    void recalc();
    void recalcFront();
    void relist();

    // processCommand
    // sendCommand
public slots:
    void processCommand(QByteArray v);

    void load();
    void save();

signals:
    void cmdOut(QByteArray);
};

#endif // MIXCALC_H
