#ifndef RKAUXFUNCTIONMANAGER_H
#define RKAUXFUNCTIONMANAGER_H

#include <QObject>
#include <QMap>
#include <QElapsedTimer>
#include <QTimer>
#include <QVector>
#include <QDebug>

#include <rkvtmanager.h>

class rkAuxAssignment : public QObject
{
    Q_OBJECT
public:
    quint8 address;
    quint16 funcId;
    quint16 inputId;
    quint64 aiuName;
    quint64 aiuModelIdCode;
    quint8 type;

    rkAuxAssignment()
    {
        funcId = 65535;
        inputId = 65535;
        aiuName = 0xFFFFFFFFFFFFFFFF;
        aiuModelIdCode = 0;
        type = 2;

        value1=0;
        value2=0;
        holdTimer.setSingleShot(true);
        connect(&holdTimer, &QTimer::timeout, this, &rkAuxAssignment::holdTimeout);
    }

    quint16 oldValue1;
    quint16 oldValue2;

    quint16 value1;
    quint16 value2;

    QTimer holdTimer;

private:
    void holdTimeout()
    {
        if(type==2)
        {
            value1 = 0;
            emit forceReleaseFromHold(funcId, value1, value2, 0);
        }
    }
signals:
    // We can connect this to the handle input slot to fake a release from hold
    void forceReleaseFromHold(quint16 funcId, quint16 value1, quint16 value2, quint8 state);
};

class rkAuxInputUnit : public QObject
{
    Q_OBJECT
public:
    rkAuxInputUnit() : QObject(0)
    {
        connect(&timer, &QTimer::timeout, this, &rkAuxInputUnit::timerTimeout);
    }

    void timerTimeout()
    {
        emit timeout(this);
    }
    rkNAME name;
    quint16 modelIdCode;
    quint8 address;
    quint8 status;
    QTimer timer;
signals:
    void timeout(rkAuxInputUnit*);
};


class rkPrefAuxInputUnit
{
public:
    rkPrefAuxInputUnit()
    {
        connected = false;
        nFunctions = 0;
    }
    rkNAME name;
    quint16 modelIdCode;
    bool connected;
    quint8 nFunctions;
};

class rkPrefAuxAssignment
{
public:
    rkNAME name;
    quint16 modelIdCode;
    quint16 funcId;
    quint16 inputId;
};

class rkAuxFunctionManager : public QObject
{
    Q_OBJECT
public:
    explicit rkAuxFunctionManager(QObject *parent = nullptr);

    rkVtManager *vt;

    void doPrefAssignment();

    void processCommand(QByteArray v);
    void processCANframe(canFrame f);
//private:          // Made public for access in ecu class for debug output
    QVector<rkPrefAuxInputUnit>prefUnits;
    QMap<quint16, rkPrefAuxAssignment>prefAssignments;  // Key is func id

    QMap<quint8, rkAuxInputUnit*> inputUnits;
    QMap<quint16, rkAuxAssignment*>assignments;      // Key is func id


    void unitTimeout(rkAuxInputUnit *v);

    void rebuildPrefUnits();

    quint8 foundUnits();

    void findTimeout();

    QTimer findTimer;
    bool prefAssignEnabled;

    void loadPrefAssignments();
    void savePrefAssignments();

    void handleInput(quint16 funcId, quint16 value1, quint16 value2, quint8 state);

    // Temp test for VT display of an analouge aux input controlling a pair of arched bar graphs
    QTimer vtUpdateSliderTimer;
    void vtUpdateSliderTimeout();
    quint16 lastP;
    quint16 curP;

    // To remove all assignments when VT is lost
    void vtLost();

signals:
    void cmdOut(QByteArray);
    void sendCANframe(canFrame);

};

#endif // RKAUXFUNCTIONMANAGER_H
