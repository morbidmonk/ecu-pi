#include "extendedtransportprotocol.h"
#include <QDebug>

extendedTransportProtocol::extendedTransportProtocol(quint8 *a, int mode) : QObject(0)
{
    ownAddress = a;
    remoteAddress = 0;
    active = false;
    connected = false;

    txMode = mode;

    pgn = 0;
    totalBytes = 0;

    currentPacket = 0;
    totalPackets = 0;
    lastPacketSize = 0;
    requestedCount = 0;
    count = 0;
    haveDPO = false;
    dpo = 0;
    onHold = false;
    maxSend = 10;
    percent = 0;
    connect(&sendTimer, SIGNAL(timeout()), this, SLOT(sendTimerTimeout()));
   // connect(this,SIGNAL(sendNext()),this,SLOT(sendTimerTimeout()));

    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), this, SLOT(proccessTimeout()));
}

void extendedTransportProtocol::processMessage(canFrame frame)
{

    // ETP.CM
    if(frame.pgn == 51200 && frame.ps == *ownAddress)
    {
        // ETP.CM_RTS
        if((uchar)frame.data[0] == 20 && !txMode)
        {
            int tpgn = (uchar)frame.data[5] | ((uchar)frame.data[6] << 8) | ((uchar)frame.data[7] << 16);
            if(connected)
            {
                if(frame.sa != remoteAddress)
                {
                    // Abort 1
                    return;
                }
                if(frame.sa == remoteAddress && tpgn != pgn)
                {
                    // Abort 1?
                    return;
                }
            }

            connected = true;
            active = true;

            remoteAddress = frame.sa;
            pgn = (uchar)frame.data[5] | ((uchar)frame.data[6] << 8) | ((uchar)frame.data[7] << 16);
            totalBytes = (uchar)frame.data[1] | ((uchar)frame.data[2] << 8) | ((uchar)frame.data[3] << 16) | ((uchar)frame.data[4] << 24);
            qDebug() << "<< ETP.CM_RTS " << "bytes:" << totalBytes;

            data.reserve(totalBytes);

            currentPacket = 1;
            totalPackets = totalBytes / 7;
            lastPacketSize = totalBytes - (totalPackets*7);
            if(lastPacketSize > 0)
                totalPackets++;
            else
                lastPacketSize = 7;

            qDebug() << "packets" << totalPackets << "lastPacketSize" << lastPacketSize;

           // int nPacketsToRequest = 10;
            requestedCount = 255;
            if(totalPackets - currentPacket < requestedCount) requestedCount = totalPackets - currentPacket;

            percent = 0;
            emit pct(0);

            count = 0;
            haveDPO = false;

            canFrame  ectsFrame;
            ectsFrame.priority = 7;
            ectsFrame.sa = *ownAddress;
            ectsFrame.ps = remoteAddress;
            ectsFrame.pf = 200;
            ectsFrame.dlc = 8;

            ectsFrame.data[0] = 21; // CTS
            ectsFrame.data[1] = requestedCount;
            ectsFrame.data[2] = currentPacket & 0xFF;
            ectsFrame.data[3] = (currentPacket & 0xFF00) >> 8;
            ectsFrame.data[4] = (currentPacket & 0xFF0000) >> 16;
            ectsFrame.data[5] = pgn & 0xFF;
            ectsFrame.data[6] = (pgn & 0xFF00) >> 8;
            ectsFrame.data[7] = (pgn & 0xFF0000) >> 16;

            emit frameToSend(ectsFrame);
            timer.start(1250);  // T2
        }
        // ETP.CM_DPO
        if((uchar)frame.data[0] == 22 && !txMode && connected && frame.sa == remoteAddress)
        {
            timer.stop();
            timer.start(750);   // T1

            int tpgn = (uchar)frame.data[5] | ((uchar)frame.data[6] << 8) | ((uchar)frame.data[7] << 16);

            if(tpgn!=pgn)
            {
                // Abort 10 DPO unexpected PGN
                return;
            }
            if(haveDPO)
            {
                // Abort 9 unexpected DPO
                return;
            }

            // Check nPackets not greater than in CTS
            // if lower then adjust requested count
            if((uchar)frame.data[1] > requestedCount)
            {
                // err
            }
            else
            {
                requestedCount = (uchar)frame.data[1];
            }
            haveDPO = true;
            dpo = (uchar)frame.data[2] | ((uchar)frame.data[3] << 8) | ((uchar)frame.data[4] << 16);

            int tpct = (double)((double)currentPacket / (double)totalPackets)*1000.0f;
            if(tpct > percent)
            {
                percent = tpct;
                emit pct(percent);
            }

            qDebug() << ">> ETP.DPO" << requestedCount;
        }

        // ETP.CTS
        if((uchar)frame.data[0] == 21 && txMode && active && frame.sa == remoteAddress)
        {
            timer.stop();
            qDebug() << ">> ETP.CTS";

            if(haveDPO)
            {
                // Abort 4       CTS when data transfer in progress
                return;
            }

            if(!connected)  emit statusMessage(QString("ETP Send Session: <font color=green><b>Connected<\b><\font>"));

            requestedCount = (uchar)frame.data[1];
            currentPacket = (uchar)frame.data[2] | ((uchar)frame.data[3] << 8) | ((uchar)frame.data[4] << 16);

            connected = true;

            if(requestedCount == 0)
            {
                if(!onHold)emit statusMessage(QString("ETP Send Session: <font color=orange><b>On Hold<\b><\font>"));
                onHold = true;
                timer.start(1050);  // T4
                return;
            }
            if(onHold)emit statusMessage(QString("ETP Send Session: <font color=green><b>Resumed<\b><\font>"));
            onHold = false;

            dpo = currentPacket - 1;

            // Send DPO
            canFrame dpoFrame;
            dpoFrame.priority = 7;
            dpoFrame.sa = *ownAddress;
            dpoFrame.ps = remoteAddress;
            dpoFrame.pf = 200;
            dpoFrame.dlc = 8;

            dpoFrame.data[0] = 22;
            dpoFrame.data[1] = requestedCount;
            dpoFrame.data[2] = (dpo) & 0xFF;
            dpoFrame.data[3] = (dpo >> 8) & 0xFF;
            dpoFrame.data[4] = (dpo >> 16) & 0xFF;
            dpoFrame.data[5] = pgn & 0xFF;
            dpoFrame.data[6] = (pgn & 0xFF00) >> 8;
            dpoFrame.data[7] = (pgn & 0xFF0000) >> 16;

            qDebug() << "<< ETP.DPO" <<  QString("%1%").arg((double)((double)currentPacket / (double)totalPackets)*100.0f,3,'f',1);;


            int tpct = (double)((double)currentPacket / (double)totalPackets)*1000.0f;
            if(tpct > percent)
            {
                percent = tpct;
                emit pct(percent);
            }


            emit frameToSend(dpoFrame);
            // Then start DT timer
            sendTimer.start(0);
            //emit sendNext();
        }

        // ACK
        if((uchar)frame.data[0]==23 && txMode && connected && frame.sa == remoteAddress)
        {
            connected = false;
            active = false;
            timer.stop();
            emit pct(1000);
            qDebug() << ">> ETP.ACK";
            emit statusMessage(QString("ETP Send Session: <font color=green><b>Completed (ACK)<\b><\font>"));

            emit sent(true);
        }
    }
    // ETP.DT
    if(frame.pgn == 50944 && connected && frame.sa == remoteAddress && frame.ps == *ownAddress)
    {
        timer.stop();

        if(!haveDPO)
        {
            // Abort 6 ( unexpected DT )
            return;
        }

        // dpo + seq = packet number
        currentPacket = dpo + frame.data[0];

        int remain = 7;
        if(currentPacket == totalPackets) remain = lastPacketSize;
        for(int i=0;i<remain;i++)
        {
            data[ ((currentPacket-1)*7) + i] = (uchar)frame.data[i+1];
        }
        count++;

        if(count == requestedCount)
        {
            if(currentPacket == totalPackets)
            {
                // EndofMessageACK

                canFrame  ackFrame;
                ackFrame.priority = 7;
                ackFrame.sa = *ownAddress;
                ackFrame.ps = remoteAddress;
                ackFrame.pf = 200;
                ackFrame.dlc = 8;

                ackFrame.data[0] = 23; // ACK
                ackFrame.data[1] = totalBytes & 0xFF;
                ackFrame.data[2] = (totalBytes & 0xFF00) >> 8;
                ackFrame.data[3] = (totalBytes & 0xFF0000) >> 16;
                ackFrame.data[4] = (totalBytes & 0xFF000000) >> 24;
                ackFrame.data[5] = pgn & 0xFF;
                ackFrame.data[6] = (pgn & 0xFF00) >> 8;
                ackFrame.data[7] = (pgn & 0xFF0000) >> 16;

                emit frameToSend(ackFrame);
                emit pct(1000);

                connected = false;
                active = false;

                qDebug() << "<< ETP.CM_EndofMessageACK";

                emit received(*this);
            }
            else
            {
                // Next CTS
                requestedCount = 255;
                if(totalPackets - currentPacket < requestedCount) requestedCount = totalPackets - currentPacket;

                count = 0;
                haveDPO = false;

                currentPacket++;

                canFrame  ectsFrame;
                ectsFrame.priority = 7;
                ectsFrame.sa = *ownAddress;
                ectsFrame.ps = remoteAddress;
                ectsFrame.pf = 200;
                ectsFrame.dlc = 8;

                ectsFrame.data[0] = 21; // CTS
                ectsFrame.data[1] = requestedCount;
                ectsFrame.data[2] = currentPacket & 0xFF;
                ectsFrame.data[3] = (currentPacket & 0xFF00) >> 8;
                ectsFrame.data[4] = (currentPacket & 0xFF0000) >> 16;
                ectsFrame.data[5] = pgn & 0xFF;
                ectsFrame.data[6] = (pgn & 0xFF00) >> 8;
                ectsFrame.data[7] = (pgn & 0xFF0000) >> 16;

                qDebug() << "<< ETP.CTS" << currentPacket;

                emit frameToSend(ectsFrame);
                timer.start(1250);      // T2
            }
        }
    }
}

void extendedTransportProtocol::send(int dest, int s_pgn, const QByteArray &s_data)
{
    //if(active) return;
    data = s_data;

    onHold = false;

    connected = false;

    totalBytes = data.count();
    totalPackets = totalBytes / 7;
    lastPacketSize = totalBytes - (totalPackets*7);
    if(lastPacketSize > 0) totalPackets++;

    emit statusMessage(QString("ETP Send Session: <font color=dodgerblue><b>Dest=%1 PGN=%2 %3KB in %4 packets<\b><\font>").arg(dest).arg(s_pgn).arg(totalBytes/1024).arg(totalPackets));

    qDebug() << "<< ETP RTS: totalBytes     " << totalBytes;
    qDebug() << "<< ETP RTS: totalPackets   " << totalPackets;
    qDebug() << "<< ETP RTS: lastPacketSize " << lastPacketSize;

    remoteAddress = dest;
    pgn = s_pgn;

    active = true;
    count = 0;

    dpo = 0;

    canFrame  rtsFrame;

    rtsFrame.sa = *ownAddress;
    rtsFrame.pf = 200;
    rtsFrame.ps = remoteAddress;
    rtsFrame.priority = 7;
    rtsFrame.dlc = 8;
    rtsFrame.data[0] = 20;  // RTS
    rtsFrame.data[1] = totalBytes & 0xFF;
    rtsFrame.data[2] = (totalBytes & 0xFF00) >> 8;
    rtsFrame.data[3] = (totalBytes & 0xFF0000) >> 16;
    rtsFrame.data[4] = (totalBytes & 0xFF000000) >> 24;
    rtsFrame.data[5] = pgn & 0xFF;
    rtsFrame.data[6] = (pgn & 0xFF00 ) >> 8;
    rtsFrame.data[7] = (pgn & 0xFF0000 ) >> 16;

    percent = 0;
    emit pct(percent);

    emit frameToSend(rtsFrame);
    timer.start(1250);      // T3
}

void extendedTransportProtocol::sendTimerTimeout()
{
    // Sequence number = currentPacket - dpo

    canFrame dtFrame;
    dtFrame.sa = *ownAddress;
    dtFrame.ps = remoteAddress;
    dtFrame.pf = 199;       // DT
    dtFrame.dlc = 8;
    dtFrame.priority = 7;

    dtFrame.data[0] = currentPacket - dpo;


    int bytesToSend = 7;
    if( (currentPacket == totalPackets) && lastPacketSize > 0)
    {
        for(int i=1;i<8;i++)
        {
            dtFrame.data[i] = 0xFF;
        }
        bytesToSend = lastPacketSize;
    }

    int address = (currentPacket-1)*7;

    for(int i = 0; i < bytesToSend ; i++)
    {
        dtFrame.data[i+1] = data[address++];
    }

    currentPacket++;
    requestedCount--;

    emit frameToSend(dtFrame);

    if(requestedCount == 0)
    {
        timer.start(1250);
        sendTimer.stop();
    }
}

void extendedTransportProtocol::proccessTimeout()
{

    if(txMode)
    {
        // CTS After RTS
        if(!connected)
        {
            emit statusMessage(QString("ETP Send Session: <font color=red><b>Connect Failed (timeout)<\b><\font>"));
            active = false;
            connected = false;
            return;
        }

        // CTS While on Hold
        if(onHold)
        {
            emit statusMessage(QString("ETP Send Session: <font color=red><b>Timeout (HOLD)<\b><\font>"));
            active = false;
            connected = false;
            return;
        }

        // CTS or ACK after DT
        if(requestedCount == 0 && !onHold)
        {
            emit statusMessage(QString("ETP Send Session: <font color=red><b>Timeout (CTS|ACK)<\b><\font>"));
            active = false;
            connected = false;
            return;
        }
    }
    else
    {
        // DPO after CTS
        // DT after DPO
        // DT after DT
    }
}
