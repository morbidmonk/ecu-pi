#ifndef RKNAME_H
#define RKNAME_H

#include <QObject>

class rkNAME
{
public:
    rkNAME();

    void setNAME(quint64);
    void setSelfConfig(quint32);
    void setIndustry(quint32);
    void setDevClassInstance(quint32);
    void setDevClass(quint32);
    void setDevFunction(quint32);
    void setDevFunctionInstance(quint32);
    void setECUInstance(quint32);
    void setManufacturer(quint32);
    void setIdentity(quint32);

    quint64 name;
    bool selfConfig;
    quint32 industry;
    quint32 devClassInstance;
    quint32 devClass;
    quint32 devFunction;
    quint32 devFunctionInstance;
    quint32 ECUInstance;
    quint32 manufacturer;
    quint32 identity;

    enum masks
    {
        noIdentity = 0xFFFFFFFFFFE00000
    };
};

#endif // RKNAME_H
