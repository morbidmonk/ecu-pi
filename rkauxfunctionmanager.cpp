#include "rkauxfunctionmanager.h"
#include <QDataStream>
#include <QDebug>
#include <QFile>

rkAuxFunctionManager::rkAuxFunctionManager(QObject *parent) : QObject(parent)
{
    vt = nullptr;
    prefAssignEnabled = false;
    connect(&findTimer, &QTimer::timeout, this, &rkAuxFunctionManager::findTimeout);

    rkPrefAuxAssignment t;
    t.funcId = 50000;
    t.inputId = 10000;
    t.name.setNAME(0xa00c840076a0cc0a /*0xa00c870076a0cc0b*/);
    t.modelIdCode = 43981;
  //  prefAssignments.insert(t.funcId, t);

    t.funcId = 50001;
    t.inputId = 10000;
    t.name.setNAME(0xa00c870076a0cc0b);
    t.modelIdCode = 43981;
  //  prefAssignments.insert(t.funcId, t);

 //   rebuildPrefUnits();

    loadPrefAssignments();

    connect(&vtUpdateSliderTimer, &QTimer::timeout, this, &rkAuxFunctionManager::vtUpdateSliderTimeout);


    lastP = 0;
    curP = 0;
}

void rkAuxFunctionManager::vtLost()
{
    prefAssignEnabled = false;
    // remove all assignments
    QMapIterator<quint16, rkAuxAssignment*> i(assignments);
    while(i.hasNext())
    {
        i.next();
        delete i.value();
    }
    assignments.clear();
}

void rkAuxFunctionManager::loadPrefAssignments()
{
    QFile file("prefAssign");
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream ds(&file);
        ds.setByteOrder(QDataStream::LittleEndian);
        quint8 nFuncs;
        ds >> nFuncs;
        for(int i=0;i<nFuncs;i++)
        {
            rkPrefAuxAssignment t;
            quint64 name;
            ds >> t.funcId >> t.inputId >> t.modelIdCode >> name;
            t.name.setNAME(name);
            prefAssignments.insert(t.funcId, t);
        }
    }
    rebuildPrefUnits();
}

void rkAuxFunctionManager::savePrefAssignments()
{
    QFile file("prefAssign");
    file.open(QIODevice::WriteOnly);
    QDataStream ds(&file);
    ds.setByteOrder(QDataStream::LittleEndian);

    ds << (quint8)prefAssignments.count();
    QMapIterator<quint16, rkPrefAuxAssignment> i(prefAssignments);
    while(i.hasNext())
    {
        i.next();
        ds << i.value().funcId << i.value().inputId << i.value().modelIdCode << i.value().name.name;
    }
    file.close();
}

void rkAuxFunctionManager::processCommand(QByteArray v)
{
    QDataStream ds(v);
    ds.setByteOrder(QDataStream::LittleEndian);

    quint8 cmd;

    ds >> cmd;

    // Handle Aux Assignment commands here as these are from VT
    if(cmd == 36)
    {
        quint64 name;
        quint8 flags;
        quint16 inputId;
        quint16 funcId;

        ds >> name >> flags >> inputId >> funcId;

        bool pref;

        if( flags & 0x80 ) pref = false;
        else pref = true;

        if( name == 0xFFFFFFFFFFFFFFFF )    // Remove an assignment
        {
            if( funcId == 0xFFFF)   // Remove All
            {

            }
            else    // Remove Specific
            {
                qDebug() << "Remove Aux Assignment: Func="<<funcId << "Store=" << pref;
                if(assignments.contains(funcId))
                {
                    rkAuxAssignment *auxAss = assignments.value(funcId);
                    assignments.remove(funcId);
                    delete  auxAss;
                }

                if(pref)
                {
                    if(prefAssignments.contains(funcId))
                    {
                        prefAssignments.remove(funcId);
                        rebuildPrefUnits();
                        savePrefAssignments();  // Do this here for now, maybe move to key off later
                    }
                }
            }
            // Now respond
        }
        else    // Assign
        {
            rkAuxAssignment *auxAss;
            if(assignments.contains(funcId))
            {
                qDebug() << "Aux Reassignment: Func=" << funcId << "Input=" << inputId << "Name=" << QString::number(name,16) << "Store=" << pref;
                auxAss = assignments.value(funcId);
            }
            else
            {
                qDebug() << "Aux Assignment: Func=" << funcId << "Input=" << inputId << "Name=" << QString::number(name,16) << "Store=" << pref;
                auxAss = new rkAuxAssignment;
                assignments.insert(funcId, auxAss);

                // Connect hold timeout signal?
                if((flags & 0x01F) == 2)
                {
                    qDebug() << "Type 2, connecting force release signal";
                    connect(auxAss, qOverload<quint16,quint16,quint16,quint8>(&rkAuxAssignment::forceReleaseFromHold),this,&rkAuxFunctionManager::handleInput);
                }

            }

            auxAss->aiuName = name;
            auxAss->type = flags & 0x01F;
            auxAss->inputId = inputId;
            auxAss->funcId = funcId;

            QMapIterator<quint8, rkAuxInputUnit*> i(inputUnits);
            while(i.hasNext())
            {
                i.next();
                if(i.value()->name.name == name)
                {
                    auxAss->address = i.key();
                    auxAss->aiuModelIdCode = i.value()->modelIdCode;
                }
            }


            if(pref)
            {
                rkPrefAuxAssignment ta;
                ta.funcId = funcId;
                ta.inputId = inputId;
                ta.name.setNAME(name);
                ta.modelIdCode = auxAss->aiuModelIdCode;
                prefAssignments.insert(funcId, ta);

                rebuildPrefUnits();
                savePrefAssignments();  // Do this here for now, maybe move to key off later
            }

            // Now Respond

        }

        // Response


        canFrame tx;
        tx.pf = 231;
        tx.ps = vt->address;
        tx.sa = vt->addressManager->address;
        tx.priority = 3;
        tx.dlc = 8;

        QDataStream os(&tx.data, QIODevice::WriteOnly);
        os.setByteOrder(QDataStream::LittleEndian);

        os << (quint8)36 << funcId << (quint8)0;

        emit sendCANframe(tx);
    }
}

void rkAuxFunctionManager::rebuildPrefUnits()
{
    prefUnits.clear();

    QMapIterator<quint16, rkPrefAuxAssignment>i(prefAssignments);

    while(i.hasNext())
    {
        i.next();
        bool hasIt = false;
        for(int n=0;n<prefUnits.count();n++)
        {
            if( (prefUnits.at(n).name.name & rkNAME::noIdentity) == (i.value().name.name & rkNAME::noIdentity))
            {
                if(prefUnits.at(n).modelIdCode == i.value().modelIdCode)
                {
                    hasIt = true;
                    prefUnits[n].nFunctions++;
                }
            }
        }

        if(!hasIt)
        {
            rkPrefAuxInputUnit tu;
            tu.modelIdCode = i.value().modelIdCode;
            tu.name.setNAME(i.value().name.name);
            tu.nFunctions=1;
            prefUnits.append(tu);
        }
    }
}

void rkAuxFunctionManager::doPrefAssignment()
{
    findTimer.setSingleShot(true);
    if(prefUnits.count() > 0)
    {
        if(foundUnits() == prefUnits.count()) findTimeout();    // All units connected
        else findTimer.start(10000); // 10 secs for now
    }
    else
    {
        if(inputUnits.count() != 0)
        {
           findTimeout();
        }
    }
    prefAssignEnabled = true;

    vtUpdateSliderTimer.start(50);
}

// Return the number of pref units that are connected to the network
quint8 rkAuxFunctionManager::foundUnits()
{
    quint8 nUnits = 0;
    for(int n=0;n<prefUnits.count();n++)
    {
        QMapIterator<quint8, rkAuxInputUnit*> i(inputUnits);
        while(i.hasNext())
        {
            i.next();
            // If name (not inc identity) and model id code match then nunits++, then find all funcs for this unit
            if((i.value()->name.name & rkNAME::noIdentity) == (prefUnits.at(n).name.name & rkNAME::noIdentity))
            {
                if(i.value()->modelIdCode == prefUnits.at(n).modelIdCode)
                {
                    nUnits++;
                    break;
                }
            }
        }
    }
    return nUnits;
}

void rkAuxFunctionManager::findTimeout()
{
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setByteOrder(QDataStream::LittleEndian);
    // Scan connected units v pref units and see how many are present
    quint8 nUnits = 0;

    ds << (quint8)34 << nUnits; // Pref Assignment Command, say 0 units for now, will come back and set to actual found later

    for(int n=0;n<prefUnits.count();n++)
    {
        QMapIterator<quint8, rkAuxInputUnit*> i(inputUnits);
        while(i.hasNext())
        {
            i.next();
            // If name (not inc identity) and model id code match then nunits++, then find all funcs for this unit
            if((i.value()->name.name & rkNAME::noIdentity) == (prefUnits.at(n).name.name & rkNAME::noIdentity))
            {
                if(i.value()->modelIdCode == prefUnits.at(n).modelIdCode)
                {
                    nUnits++;
                    ds << i.value()->name.name << i.value()->modelIdCode << prefUnits.at(n).nFunctions;

                    // Now scan all pref functions and match to pref inout unit
                    QMapIterator<quint16,rkPrefAuxAssignment>pf(prefAssignments);
                    while(pf.hasNext())
                    {
                        pf.next();
                        if(pf.value().modelIdCode == prefUnits.at(n).modelIdCode)
                        {
                            if(pf.value().name.name == prefUnits.at(n).name.name)
                            {
                                ds << pf.value().funcId << pf.value().inputId;
                            }
                        }
                    }

                    break;
                }
            }
        }
    }
    data[1] = nUnits;

    vt->queue_cmd(data);
}

void rkAuxFunctionManager::processCANframe(canFrame f)
{
    if(f.pgn != 59136 && f.ps !=255) return;  // Not ECU to VT with Global Destination

    // We are basically looking for Aux Input Maintenance and Aux Input Status Messages

    QDataStream ds(f.data);
    ds.setByteOrder(QDataStream::LittleEndian);

    quint8 cmd;
    ds >> cmd;

    // Auxiliary Input Maintenance
    if(cmd == 35)
    {
        quint16 mic;
        quint8 status;

        ds >> mic >> status;

        // Is it in our list of units? if so restart timer
        // if not and status is 1 then add to unit list and start timer, if we previously had 0 units send pref assignments if we have none or if this matches the pref unit?

        if(inputUnits.contains(f.sa)) // If the address is there the yeh
        {
            rkAuxInputUnit *unit = inputUnits.value(f.sa);
            unit->timer.start(300);
        }
        else
        {
            if(status == 1)
            {
                qDebug() << "Aux Input Unit Connected: " << f.sa;

                rkAuxInputUnit *unit = new rkAuxInputUnit;
                unit->address = f.sa;
                // unit->name = ;   // Lookup name in address table
                for(int i=0; i<vt->addressManager->addressTable.size();i++)
                {
                    // VT = Group 2, Class 0, Function 29, Func Instance = vt

                    rkClaimedAddress ca = vt->addressManager->addressTable[i];
                    if(ca.address == f.sa)
                    {
                        unit->name = ca.name;
                        break;
                    }
                }
                // ? If name not found do we send a request for address claimed?

                unit->modelIdCode = mic;

                unit->status = status;

                inputUnits.insert(f.sa, unit);

                // Connect units timeout to our timeout handler so we can then remove unit from list
                connect(unit, qOverload<rkAuxInputUnit*>(&rkAuxInputUnit::timeout), this, &rkAuxFunctionManager::unitTimeout);
                unit->timer.start(300);

                // Run pref assignment test?
                    // If we didn't have a unit before and we have no prefs send empty pref msg
                    // if initial boot timer hasn't expired (e.g looking for pref units on startup)
                        // if unit is a pref unit tag it as present, if all pref units are now present then send pref msg

                    // if unit is a pref unit, then send pref msg for all found units

                if(prefAssignEnabled)
                {
                    if(prefUnits.count() == 0 && inputUnits.count() == 1)
                    {
                        // No pref assignments and this is first unit we've seen, so send empty prefs
                        QByteArray data;
                        data.fill(0xFF,8);
                        QDataStream os(&data, QIODevice::WriteOnly);
                        os.setByteOrder(QDataStream::LittleEndian);
                        os << (quint8)34 << (quint8) 0;
                        vt->queue_cmd(data);
                    }
                    else
                    {
                        if(findTimer.isActive())
                        {
                            if(foundUnits() == prefUnits.count())
                            {
                                findTimer.stop();
                                findTimeout();
                            }
                        }
                        else
                        {
                            // Is it a preferred unit? If so send prefs


                            for(int n=0;n<prefUnits.count();n++)
                            {

                                if((unit->name.name & rkNAME::noIdentity) == (prefUnits.at(n).name.name & rkNAME::noIdentity))
                                {
                                    if(unit->modelIdCode == prefUnits.at(n).modelIdCode)
                                    {
                                        findTimeout();
                                    }
                                }

                            }




                        }
                    }
                }
            }
        }
    }

    // Auxiliary Input Type 2 Status
    if(cmd == 38)
    {
        quint16 inputId;
        quint16 value1;
        quint16 value2;
        quint8 state;

        ds >> inputId >> value1 >> value2 >> state;

        QMapIterator<quint16, rkAuxAssignment*> i(assignments);
        while(i.hasNext())
        {
            i.next();
            rkAuxAssignment *aux = i.value();
            if(aux->address == f.sa)
            {
                if(aux->inputId == inputId)
                {
                    // We have an input, emit a signal or handle in a func and emit any signals after decode???

                    aux->oldValue1 = aux->value1;
                    aux->oldValue2 = aux->value2;
                    aux->value1 = value1;
                    aux->value2 = value2;

                    if(aux->type==2)
                    {
                        if(value1 == 2)
                        {
                            aux->holdTimer.start(300);
                        }
                        else
                        {
                            aux->holdTimer.stop();
                        }
                    }

                    handleInput(aux->funcId, value1, value2, state);
                }
            }
        }
    }
}

void rkAuxFunctionManager::unitTimeout(rkAuxInputUnit *v)
{
    qDebug() << "Aux Input Unit Timeout: " << v->address;
    inputUnits.remove(v->address);
    delete v;
}

void rkAuxFunctionManager::handleInput(quint16 funcId, quint16 value1, quint16 value2, quint8 state)
{

    if(funcId == 261) // Slider Test
    {
       /* qint64 e = sinceLastSlider.elapsed();
        sinceLastSlider.start();
        if(e < 100) return;
        sinceLastSlider.restart();*/

        double pct = 0.0015564 * value1;
        int p = (int)pct;
        curP = p;
    }

    if(funcId == 50000)
    {
        rkAuxAssignment *aux = assignments.value(funcId);

        // Release from Pressed
        if(aux->oldValue1 == 1 && value1 == 0)
        {
            vt->queue_cmd(VT_Commands::ChangeAttribute(270, 3, 12));
            vt->queue_cmd(VT_Commands::ChangeAttribute(271, 3, 12));
        }
        // Release from Hold
        if(aux->oldValue1 == 2 && value1 == 0)
        {
            vt->queue_cmd(VT_Commands::ChangeAttribute(270, 3, 28));
            vt->queue_cmd(VT_Commands::ChangeAttribute(271, 3, 28));
        }
    }

    if(funcId == 50001)
    {
        rkAuxAssignment *aux = assignments.value(funcId);

        quint32 p;
        if(curP == 50) p = 0;
        if(curP < 50) p = 100-curP*2;
        if(curP > 50) p = (curP-50)*2;

        // Release from Pressed
        if(aux->oldValue1 == 1 && value1 == 0)
        {
            if(assignments.contains(261))
            {
                vt->queue_cmd(VT_Commands::ChangeNumericValue(286, p));
            }
        }
        // Release from Hold
        if(aux->oldValue1 == 2 && value1 == 0)
        {
            if(assignments.contains(261))
            {
                vt->queue_cmd(VT_Commands::ChangeNumericValue(286, 100));
            }
        }
    }
}

void rkAuxFunctionManager::vtUpdateSliderTimeout()
{
    if(curP != lastP)
    {
        lastP = curP;
        int p = curP;
        if(p == 50)
        {
            vt->cmd_ChangeNumericValue(284,0);
            vt->cmd_ChangeNumericValue(285,0);
        }
        if(p < 50)
        {
            vt->cmd_ChangeNumericValue(284,100-p*2);
            vt->cmd_ChangeNumericValue(285,0);
        }
        if(p>50)
        {
            vt->cmd_ChangeNumericValue(284,0);
            vt->cmd_ChangeNumericValue(285,(p-50)*2);
        }
    }
}
