#ifndef RKNATIVEEVENTFILTER_H
#define RKNATIVEEVENTFILTER_H

#include <QAbstractNativeEventFilter>
#include <QObject>

#ifdef _WIN32
    #include "windows.h"
    #include "Dbt.h"
#endif

class rkNativeEventFilter : public QAbstractNativeEventFilter
{
public:
  rkNativeEventFilter();
  bool nativeEventFilter(const QByteArray &eventType, void *message, long *) override;
};

#endif // RKNATIVEEVENTFILTER_H
