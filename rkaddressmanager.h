#ifndef RKADDRESSMANAGER_H
#define RKADDRESSMANAGER_H

// STATES
// -1 = Error , e.g couldn't find an available adddress to claim
// 0 = initial state
// 1 = Building Address Table
// 2 = claimed sent, waiting 250ms for any counter claim
// 3 = claimed and ok to do stuff (in this state a call to haveAddress will return true

#include <QObject>

#include <canframe.h>
#include <QVector>
#include <QTimer>
#include <rkclaimedaddress.h>

class rkAddressManager : public QObject
{
    Q_OBJECT
public:
    explicit rkAddressManager(QObject *parent = nullptr);
    void claimAddress(quint8 ia);
    QVector<rkClaimedAddress> addressTable;
    bool haveAddress();
    rkNAME name;
    quint8 address;
public slots:
    void processCANframe(canFrame f);
private:
    quint8 initAddr;
    quint8 curAddr;
    int state;
    QTimer timer;
private slots:
    void timeout();
signals:
    void sendCANframe(canFrame);
    void claimed(bool);
};

#endif // RKADDRESSMANAGER_H
